﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RoomGenerator {


    public static void InstansiateRooms(DungeonData dungeonData, LevelData levelData, GameObject parent)
    {
        for (int i = 0; i < dungeonData.roomArray.Length; i++)
        {
            InstansiateRoom(levelData.roomSet[0].floor,
                            new Vector3(dungeonData.roomArray[i].location.x * 3.5f - DungeonGenerator.dungeonMapSize * 1.75f, 0,
                                dungeonData.roomArray[i].location.y * 3.5f - DungeonGenerator.dungeonMapSize * 1.75f), new Vector3(dungeonData.roomArray[i].size.x * 3.5f, 4 * 3.5f, dungeonData.roomArray[i].size.y * 3.5f), 
                            new Vector3(0, 0, 0), parent);
        }
    }

    private static void InstansiateRoom(GameObject tileObject, Vector3 position, Vector3 scale, Vector3 rotation, GameObject parent)
    {
        GameObject newObj = GameObject.Instantiate(tileObject, position, Quaternion.Euler(rotation), parent.transform);
        newObj.transform.localScale = scale;
        newObj.transform.SetParent(parent.transform);
        newObj.transform.position = newObj.transform.position + new Vector3(scale.x/2 - 1.75f, 0, scale.z/2 - 1.75f);

    }
}
