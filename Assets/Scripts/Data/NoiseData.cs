﻿using UnityEngine;
using System.Collections;



[CreateAssetMenu()]
public class NoiseData : ScriptableObject
{
    public event System.Action OnValuesUpdated;
    public bool autoUpdate;

    public Noise.NormalizeMode noiseNormalizeMode;
    public float noiseScale;
    public int noiseOctaves;
    [Range(0, 1)] public float noisePersistance;
    public float noiseLacunarity;
    public int noiseSeed;
    public Vector2 noiseOffset;
    public AnimationCurve noiseMultiplierCurve;

    //[HideInInspector]
    public float[] sampledCurve;


    [HideInInspector] public Texture2D previewNoise;

    protected void OnValidate()
    {
        sampledCurve = new float[MapGenerator.mapActualHeight];
        for (int i = 0; i < MapGenerator.mapActualHeight; i++)
        {
            float heightIndex = (float)i/MapGenerator.mapActualHeight;
            sampledCurve[i] = Mathf.Clamp01(noiseMultiplierCurve.Evaluate(heightIndex));
        }


        if (noiseLacunarity < 1)
        {
            noiseLacunarity = 1;
        }

        if (noiseOctaves < 0)
        {
            noiseOctaves = 0;
        }

        if (autoUpdate)
        {
            CreateEditorPreviewTextures();
            NotifyOfUpdatedValues();
        }

    }


    public void NotifyOfUpdatedValues()
    {
        if (OnValuesUpdated != null)
        {
            OnValuesUpdated();
        }
    }

    public void CreateEditorPreviewTextures()
    {
        float[,] noiseArray = Noise.GenerateNoiseMap(MapGenerator.mapActualChunkSizePlusOne,
            MapGenerator.mapActualChunkSizePlusOne, noiseSeed, noiseScale, noiseOctaves, noisePersistance,
            noiseLacunarity, noiseOffset,
            noiseNormalizeMode, sampledCurve, 1);
        previewNoise = TextureGenerator.TextureFromHeightMap(noiseArray);

    }
}
