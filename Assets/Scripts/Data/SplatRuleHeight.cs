﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu()]
public class SplatRuleHeight : SplatRuleBase {

    public AnimationCurve heightStrength;

    //[HideInInspector]
    public float[] sampledCurve;

    protected override void OnValidate()
    {

        sampledCurve = new float[MapGenerator.mapActualHeight];
        for (int i = 0; i < MapGenerator.mapActualHeight; i++)
        {
            float heightIndex = (float)i / MapGenerator.mapActualHeight;
            sampledCurve[i] = Mathf.Clamp01(heightStrength.Evaluate(heightIndex));
        }

        if (autoUpdate)
        {
            NotifyOfUpdatedValues();
        }

    }

}
