﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class LevelData : ScriptableObject
{
    public event System.Action OnValuesUpdated;
    public bool autoUpdate;
    public bool enableForGenerator;

    [Header("Level Settings")]
    public LevelSettingsData levelSettingsData;
    public int levelSeed;

    public CorridorSet corridorSet;

    public RoomSet[] roomSet;

    public RoomSet entryRoom; //change?

    public RoomSet exitRoom; //change?

    [HideInInspector]
    public Texture2D previewLevel;

    protected void OnValidate()
    {
        if (autoUpdate)
        {
            CreateEditorPreviewTextures();
            NotifyOfUpdatedValues();
        }

    }

    public void NotifyOfUpdatedValues()
    {
        if (OnValuesUpdated != null)
        {
            OnValuesUpdated();
        }
    }



    public void CreateEditorPreviewTextures()
    {
        Texture2D texture = TextureGenerator.TextureFromByteMap(LevelGenerator.GenerateLayoutMap(new Point(DungeonGenerator.dungeonMapSize/2, DungeonGenerator.dungeonMapSize / 2), levelSettingsData, levelSeed).roomMap);
        previewLevel = texture;

        GameObject previewPlane = GameObject.FindGameObjectWithTag("previewPlane");
        Renderer rend = previewPlane.GetComponent<Renderer>();
        rend.sharedMaterial.mainTexture = texture;
        
    }


}

[System.Serializable]
public struct CorridorSet
{
    public GameObject xAxisCorridor;
    public GameObject zAxisCorridor;
    public GameObject curveCorridor;
    public GameObject tShapeCorridor;

}


[System.Serializable]
public struct RoomSet
{
    public GameObject floor;
    public GameObject ceiling;
    public GameObject wall;
    
}
