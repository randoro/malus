﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class ClimateData : ScriptableObject {

    [Header("Strength Settings")]
    public NoiseData strengthData;

    public string climateName;
    public string climateMinName;
    public string climateMaxName;


}
