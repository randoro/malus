﻿using UnityEngine;
using System.Collections;
using System.Runtime.Remoting.Messaging;

[CreateAssetMenu()]
public class SplatRuleNormal : SplatRuleBase {

    public Vector2 normalVector;
    public float maxErrorInDegrees;

    private const double oneDegreeInRadians = 0.01745329252;


    public float hasRightNormal(Vector3 trynormal)
    {
        Vector3 _trynormal = new Vector2(trynormal.x, trynormal.z);
        float mags = _trynormal.magnitude * normalVector.magnitude;
        if (mags > 0)
        {
            float dot = Vector2.Dot(_trynormal, normalVector);
            if (dot > 0)
            {
                float error = Mathf.Acos(dot / mags);
                if (error < (maxErrorInDegrees*oneDegreeInRadians))
                {
                    return 1;
                }
            }
        }

        return 0;
    }
}
