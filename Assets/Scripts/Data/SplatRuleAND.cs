﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class SplatRuleAND : SplatRuleBase
{
    public SplatRuleHeight heightRule;
    public SplatRuleNormal normalRule;
    
}
