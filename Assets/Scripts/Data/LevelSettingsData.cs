﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class LevelSettingsData : ScriptableObject {

    public event System.Action OnValuesUpdated;
    public bool autoUpdate;

    [Range(1, 500)]
    public int maxTries;
    [Range(5, 100)]
    public int maxChainLength;
    [Range(1, 400)]
    public int maxRooms;

    [Range(0, 1)]
    public int branchness;

    [Range(1, 5)]
    public int corridorCrawliness;

    public bool entranceToNextLevel;


    [HideInInspector]
    public Texture2D previewLevel;


    protected void OnValidate()
    {
        if (autoUpdate)
        {
            NotifyOfUpdatedValues();
        }

    }

    public void NotifyOfUpdatedValues()
    {
        if (OnValuesUpdated != null)
        {
            OnValuesUpdated();
        }
    }

    public void CreateEditorPreviewTextures()
    {
        Texture2D texture = TextureGenerator.TextureFromByteMap(LevelGenerator.GenerateLayoutMap(new Point(DungeonGenerator.dungeonMapSize / 2, DungeonGenerator.dungeonMapSize / 2), this, Random.Range(0, 10000)).roomMap);
        previewLevel = texture;

        GameObject previewPlane = GameObject.FindGameObjectWithTag("previewPlane");
        Renderer rend = previewPlane.GetComponent<Renderer>();
        rend.sharedMaterial.mainTexture = texture;

    }
}
