﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu()]
public class BiomeData : ScriptableObject
{
    public event System.Action OnValuesUpdated;
    public bool autoUpdate;
    public bool enableForGenerator;

    [Range(0.0f, 1.0f)]
    public float overrideIntensity;
    
    [Header("Noise Settings")]
    public NoiseData noiseData;
    public int noiseSeed;
    [Range(0.0f, 1.0f)]
    public float noiseIntensity;


    public ClimateSet[] ClimateSet;

    [Header("Strength Settings")]
    public NoiseData strengthData;
    public int strengthSeed;
    [Range(0.0f, 1.0f)]
    public float strengthIntensity;

    public TextureSet[] textures;

    public ObjectSet[] objects;

    [HideInInspector]
    public Texture2D previewNoise;

    [HideInInspector]
    public Texture2D previewStrength;

    [HideInInspector]
    public bool preview;

    protected void OnValidate()
    {

        if (autoUpdate)
        {
            CreateEditorPreviewTextures();
            NotifyOfUpdatedValues();
        }
        
    }
    
    public void NotifyOfUpdatedValues()
    {
        if (OnValuesUpdated != null)
        {
            OnValuesUpdated();
        }
    }

    public void CreateEditorPreviewTextures()
    {
        float[,] noiseArray = Noise.GenerateNoiseMap(MapGenerator.mapActualChunkSizePlusOne,
                MapGenerator.mapActualChunkSizePlusOne, noiseData.noiseSeed + noiseSeed, noiseData.noiseScale, noiseData.noiseOctaves, noiseData.noisePersistance, noiseData.noiseLacunarity, noiseData.noiseOffset,
                noiseData.noiseNormalizeMode, noiseData.sampledCurve, noiseIntensity * overrideIntensity);
        previewNoise = TextureGenerator.TextureFromHeightMap(noiseArray);

        float[,] strengthArray = Noise.GenerateNoiseMap(MapGenerator.mapActualChunkSizePlusOne,
            MapGenerator.mapActualChunkSizePlusOne, strengthData.noiseSeed + strengthSeed, strengthData.noiseScale, strengthData.noiseOctaves, strengthData.noisePersistance, strengthData.noiseLacunarity, strengthData.noiseOffset,
                strengthData.noiseNormalizeMode, strengthData.sampledCurve, strengthIntensity * overrideIntensity);
        previewStrength = TextureGenerator.TextureFromHeightMap(strengthArray);
    }
}

[System.Serializable]
public struct TextureSet
{
    public Texture2D texture;
    public Texture2D normalMap;
    //public AnimationCurve heightStrength;
    [Range(1, 2048)]
    public int textureWidth;
    [Range(1, 2048)]
    public int textureHeight;

    [Space(10)]
    public SplatRuleBase splatRule;
    
    [Header("Texture & seed will be ignored if texture not defined.")]
    public NoiseData textureStrength;
    public int strengthSeed;
    [Range(0.0f, 1.0f)]
    public float intensity;
}

public enum CollisionType { ignoresButLeavesMark, checksAndLeavesMark, ignoresFull }

[System.Serializable]
public struct ObjectSet
{
    public GameObject prefabReference;
    //public Vector3 scale //maybe later for same object different scales?
    public float chanceOfGettingPicked;
    [Range(0.0f, 1.0f)]
    public float concentration;
    [Space(10)]

    public bool useRandomRotation;
    public bool useRandomScaleOffset;
    [Range(0.0f, 1.0f)]
    public float randomScaleOffset;
    public float heightOffset;
    public CollisionType collisionType;
    
    [Space(10)]
    public SplatRuleBase splatRule;
    
    [Header("Texture & seed will be ignored if texture not defined.")]
    public NoiseData textureStrength;
    public int strengthSeed;
    [Range(0.0f, 1.0f)]
    public float intensity;
}

[System.Serializable]
public struct ClimateSet
{
    [Range(0.0f, 1.0f)]
    public float climateIndicator;
}