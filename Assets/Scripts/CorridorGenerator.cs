﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CorridorGenerator {


    public static void InstansiateCorridors(DungeonData dungeonData, LevelData levelData, GameObject parent)
    {
        for (int i = 0; i < dungeonData.roomMap.GetLength(1); i++)
        {
            for (int j = 0; j < dungeonData.roomMap.GetLength(0); j++)
            {
                if (dungeonData.roomMap[j, i] == 128)
                {
                    bool tileUp = IsTileCorridorOrRoom(new Point(j, i - 1), dungeonData.roomMap);
                    bool tileDown = IsTileCorridorOrRoom(new Point(j, i + 1), dungeonData.roomMap);
                    bool tileLeft = IsTileCorridorOrRoom(new Point(j - 1, i), dungeonData.roomMap);
                    bool tileRight = IsTileCorridorOrRoom(new Point(j + 1, i), dungeonData.roomMap);

                    if (tileUp && tileDown)
                    {
                        //Straight Vert
                        InstansiateCorridorTile(levelData.corridorSet.xAxisCorridor,
                            new Vector3(j*3.5f - DungeonGenerator.dungeonMapSize*1.75f, 0,
                                i*3.5f - DungeonGenerator.dungeonMapSize*1.75f),
                            new Vector3(0, 0, 0), parent);
                    }

                    if (tileLeft && tileRight)
                    {
                        //Straight Horiz
                        InstansiateCorridorTile(levelData.corridorSet.xAxisCorridor,
                            new Vector3(j*3.5f - DungeonGenerator.dungeonMapSize*1.75f, 0,
                                i*3.5f - DungeonGenerator.dungeonMapSize*1.75f),
                            new Vector3(0, 90, 0), parent);
                    }

                    if (tileUp && tileLeft)
                    {
                        //Bend1
                        InstansiateCorridorTile(levelData.corridorSet.curveCorridor,
                            new Vector3(j*3.5f - DungeonGenerator.dungeonMapSize*1.75f, 0,
                                i*3.5f - DungeonGenerator.dungeonMapSize*1.75f),
                            new Vector3(0, 0, 0), parent);
                    }

                    if (tileUp && tileRight)
                    {
                        //Bend2
                        InstansiateCorridorTile(levelData.corridorSet.curveCorridor,
                            new Vector3(j*3.5f - DungeonGenerator.dungeonMapSize*1.75f, 0,
                                i*3.5f - DungeonGenerator.dungeonMapSize*1.75f),
                            new Vector3(0, -90, 0), parent);
                    }

                    if (tileDown && tileLeft)
                    {
                        //Bend3
                        InstansiateCorridorTile(levelData.corridorSet.curveCorridor,
                            new Vector3(j*3.5f - DungeonGenerator.dungeonMapSize*1.75f, 0,
                                i*3.5f - DungeonGenerator.dungeonMapSize*1.75f),
                            new Vector3(0, 90, 0), parent);

                    }

                    if (tileDown && tileRight)
                    {
                        //Bend4
                        InstansiateCorridorTile(levelData.corridorSet.curveCorridor,
                            new Vector3(j*3.5f - DungeonGenerator.dungeonMapSize*1.75f, 0,
                                i*3.5f - DungeonGenerator.dungeonMapSize*1.75f),
                            new Vector3(0, 180, 0), parent);

                    }
                }
            }
        }
    }


    private static void InstansiateCorridorTile(GameObject tileObject, Vector3 position, Vector3 rotation, GameObject parent)
    {
        GameObject.Instantiate(tileObject, position, Quaternion.Euler(rotation), parent.transform);
    }



    static bool IsTileCorridorOrRoom(Point tile, byte[,] layerArray)
    {
        if (tile.x < 0 || tile.y < 0 || tile.x >= layerArray.GetLength(0) ||
            tile.y >= layerArray.GetLength(1))
        {
            //Outside layerArray
            return false;
        }

        if (layerArray[tile.x, tile.y] == 255 || layerArray[tile.x, tile.y] == 128)
        {
            //Intersects room or corridor
            return true;
        }

        return false;
    }
}
