﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EndlessTerrain : MonoBehaviour {

    
    const float viewerMoveThresholdForChunkUpdate = 64f;
    const float sqrViewerMoveThresholdForChunkUpdate = viewerMoveThresholdForChunkUpdate* viewerMoveThresholdForChunkUpdate;

    public Transform viewer;
    public Material mapMaterial;

    public static Vector2 viewerPosition;
    Vector2 viewerPositionOld;
    static MapGenerator mapGenerator;
    //int chunksVisibleInViewDst;

    Dictionary<Vector2, TerrainChunk> terrainChunkDictionary = new Dictionary<Vector2, TerrainChunk>();
    //static List<TerrainChunk> terrainChunksVisibleLastUpdate = new List<TerrainChunk>();
    

    public GameObject[] prefabs = new GameObject[9];

    public Vector2[] oldPositions =
    {
        new Vector2(123, 123), new Vector2(123, 123), new Vector2(123, 123),
        new Vector2(123, 123), new Vector2(123, 123), new Vector2(123, 123), new Vector2(123, 123),
        new Vector2(123, 123), new Vector2(123, 123)
    };

    void Start()
    {
        mapGenerator = FindObjectOfType<MapGenerator>();
        
        UpdateVisibleChunks();
    }

    void Update()
    {
        viewerPosition = new Vector2(viewer.position.x, viewer.position.z);

        if((viewerPositionOld - viewerPosition).sqrMagnitude > sqrViewerMoveThresholdForChunkUpdate)
        {
            Debug.Log("Moved from " + viewerPositionOld + " to " + viewerPosition);
            viewerPositionOld = viewerPosition;
            UpdateVisibleChunks();
        }
    }

    int CalculatePrefabIndex(Vector2 viewedChunkCoord)
    {
        int prefabX = 0;
        int prefabY = 0;
        
        if (viewedChunkCoord.x >= 0)
        {
            prefabX = (int)viewedChunkCoord.x % 3;
        }
        else
        {
            prefabX = (int)(3 + (viewedChunkCoord.x % 3)) % 3;
        }

        if (viewedChunkCoord.y >= 0)
        {
            prefabY = (int)viewedChunkCoord.y % 3;
        }
        else
        {
            prefabY = (int)(3 + (viewedChunkCoord.y % 3)) % 3;
        }
        
        int prefabIndex = (prefabY * 3 + prefabX) % 9;
        return prefabIndex;
    }


    public void ConnectNeighbours(int[,] currentPrefabIndex)
    {
        Terrain terrain0 = prefabs[currentPrefabIndex[0, 0]].GetComponent<Terrain>();
        Terrain terrain1 = prefabs[currentPrefabIndex[1, 0]].GetComponent<Terrain>();
        Terrain terrain2 = prefabs[currentPrefabIndex[2, 0]].GetComponent<Terrain>();
        Terrain terrain3 = prefabs[currentPrefabIndex[0, 1]].GetComponent<Terrain>();
        Terrain terrain4 = prefabs[currentPrefabIndex[1, 1]].GetComponent<Terrain>();
        Terrain terrain5 = prefabs[currentPrefabIndex[2, 1]].GetComponent<Terrain>();
        Terrain terrain6 = prefabs[currentPrefabIndex[0, 2]].GetComponent<Terrain>();
        Terrain terrain7 = prefabs[currentPrefabIndex[1, 2]].GetComponent<Terrain>();
        Terrain terrain8 = prefabs[currentPrefabIndex[2, 2]].GetComponent<Terrain>();

        terrain0.SetNeighbors(null, terrain3, terrain1, null);
        terrain1.SetNeighbors(terrain0, terrain4, terrain2, null);
        terrain2.SetNeighbors(terrain1, terrain5, null, null);

        terrain3.SetNeighbors(null, terrain6, terrain4, terrain0);
        terrain4.SetNeighbors(terrain3, terrain7, terrain5, terrain1);
        terrain5.SetNeighbors(terrain4, terrain8, null, terrain2);

        terrain6.SetNeighbors(null, null, terrain7, terrain3);
        terrain7.SetNeighbors(terrain6, null, terrain8, terrain4);
        terrain8.SetNeighbors(terrain7, null, null, terrain5);

        terrain0.Flush();
        terrain1.Flush();
        terrain2.Flush();

        terrain3.Flush();
        terrain4.Flush();
        terrain5.Flush();

        terrain6.Flush();
        terrain7.Flush();
        terrain8.Flush();


    }

    void UpdateVisibleChunks()
    {
        

        int currentChunkCoordX = Mathf.RoundToInt((viewerPosition.x - MapGenerator.mapActualChunkSize / 2 )/ MapGenerator.mapActualChunkSize);
        int currentChunkCoordY = Mathf.RoundToInt((viewerPosition.y - MapGenerator.mapActualChunkSize / 2 )/ MapGenerator.mapActualChunkSize);

        Vector2[] newPositions = new Vector2[9];

        int[,] currentPrefabIndex = new int[3,3];
        
        Debug.Log("To chunkX: " + currentChunkCoordX + " chunkY " + currentChunkCoordY);
        for (int yOffset = -1; yOffset <= 1; yOffset++)
        {
            for (int xOffset = -1; xOffset <= 1; xOffset++)
            {
                Vector2 viewedChunkCoord = new Vector2(currentChunkCoordX + xOffset, currentChunkCoordY + yOffset);
                int prefabIndex = CalculatePrefabIndex(viewedChunkCoord);
                currentPrefabIndex[xOffset + 1, yOffset + 1] = prefabIndex;

                //if (oldPositions[prefabIndex] != null)
                //{
                //    if (terrainChunkDictionary.ContainsKey(oldPositions[prefabIndex]))
                //    {
                //        terrainChunkDictionary[oldPositions[prefabIndex]].DestroyTerrain();
                //        //terrainChunkDictionary.Remove(oldPositions[prefabIndex]);
                //    }
                //}


                if (terrainChunkDictionary.ContainsKey(viewedChunkCoord))
                {
                    Debug.Log("To prefab: " + prefabs[prefabIndex].transform.position + " checker " + viewedChunkCoord * MapGenerator.mapActualChunkSize);
                    if (prefabs[prefabIndex].transform.position.x != viewedChunkCoord.x* MapGenerator.mapActualChunkSize ||
                        prefabs[prefabIndex].transform.position.z != viewedChunkCoord.y* MapGenerator.mapActualChunkSize)
                    {
                        prefabs[prefabIndex].transform.position = new Vector3(viewedChunkCoord.x * MapGenerator.mapActualChunkSize, 0, viewedChunkCoord.y * MapGenerator.mapActualChunkSize);
                        terrainChunkDictionary[viewedChunkCoord].UpdatePrefab(prefabs[prefabIndex]);
                        terrainChunkDictionary[viewedChunkCoord].UpdateTerrainChunk();
                        terrainChunkDictionary[viewedChunkCoord].UpdateSplatChunk();
                        terrainChunkDictionary[viewedChunkCoord].UpdateObjectChunk();
                    }
                }
                else
                {
                    prefabs[prefabIndex].transform.position = new Vector3(viewedChunkCoord.x * MapGenerator.mapActualChunkSize, 0, viewedChunkCoord.y * MapGenerator.mapActualChunkSize);

                    terrainChunkDictionary.Add(viewedChunkCoord, new TerrainChunk(prefabs[prefabIndex], viewedChunkCoord, MapGenerator.mapActualChunkSize, transform, mapMaterial));
                }

                newPositions[prefabIndex] = viewedChunkCoord;
            }
        }

        ConnectNeighbours(currentPrefabIndex);

        for (int i = 0; i < 9; i++)
        {
            bool stillInUse = false;
            for (int j = 0; j < 9; j++)
            {
                if (oldPositions[i].x == newPositions[j].x && oldPositions[i].y == newPositions[j].y)
                {
                    stillInUse = true;
                    break;
                }
            }

            if (!stillInUse)
            {
                terrainChunkDictionary[oldPositions[i]].DestroyTerrain();
            }
        }

        oldPositions = newPositions;



    }


    public class TerrainChunk {

        GameObject prefab;
        Vector2 position;
        
        MapData mapData;
        SplatData splatData;
        ObjectData[] objectDatas;
        bool mapDataReceived;
        bool splatMapReceived;

        private GameObject[] terrainObjects;
        private int terrainObjectIndex = 0;

        private bool isSpawningObjects;
        private bool isDoneSpawningObjects;

        private bool isDestroyingObjects;
        private bool isDoneDestroyingObjects;



        public TerrainChunk(GameObject prefab, Vector2 coord, int size, Transform parent, Material material)
        {

            this.prefab = prefab;
            position = coord * size;
            
            SetVisible(false);

            
            mapGenerator.RequestMapData(position, OnMapDataReceived);
        }


        public void DestroyTerrain()
        {
            mapGenerator.StopCoroutine(SpawnGameObjects());
            mapGenerator.StartCoroutine(DestroyGameObjects());
        }

        public void UpdatePrefab(GameObject prefab)
        {
            this.prefab = prefab;
        }

        void OnMapDataReceived(MapData mapData)
        {
            this.mapData = mapData;
            mapDataReceived = true;

            mapGenerator.RequestSplatData(mapData, OnSplatDataReceived);
            mapGenerator.RequestObjectDatas(mapData, OnObjectDataReceived);

            UpdateTerrainChunk();
        }


        public void UpdateTerrainChunk()
        {
            if (mapDataReceived)
            {
                TerrainGenerator.GenerateTerrainData(ref prefab, mapData.heightMap, mapGenerator);
            }

        }

        void OnSplatDataReceived(SplatData splatData)
        {
            this.splatData = splatData;
            splatMapReceived = true;

            UpdateSplatChunk();

            SetVisible(true);
        }

        public void UpdateSplatChunk()
        {
            if (splatMapReceived)
            {
                TerrainGenerator.GenerateTerrainSplat(ref prefab, mapGenerator.biomeData, splatData.splatMap, mapGenerator);
                
            }
        }

        public void UpdateObjectChunk()
        {
            mapGenerator.RequestObjectDatas(mapData, OnObjectDataReceived);
        }

        void OnObjectDataReceived(ObjectData[] objectDatas)
        {
            this.objectDatas = objectDatas;
            //InstantiateObjectDataArray();
            //mapGenerator.StartCoroutine(DestroyGameObjects());

            mapGenerator.StartCoroutine(SpawnGameObjects());

        }


        public IEnumerator SpawnGameObjects()
        {
            if (objectDatas == null || objectDatas.Length == 0)
            {
                terrainObjectIndex = 0;
                //isSpawningObjects = false;
                //isDoneSpawningObjects = true;
                yield break;
            }
            terrainObjects = new GameObject[objectDatas.Length];

            while (true)
            {
                
                //if (isDestroyingObjects && !isDoneDestroyingObjects)
                //{
                //    yield return null;
                //}


                //isSpawningObjects = true;
                //isDoneSpawningObjects = false;

                GameObject newObject = (GameObject) GameObject.Instantiate(
                    mapGenerator.biomeData[objectDatas[terrainObjectIndex].biomeDataIndex].objects[
                        objectDatas[terrainObjectIndex].objectDataIndex].prefabReference,
                    new Vector3(objectDatas[terrainObjectIndex].position.x + position.x,
                        objectDatas[terrainObjectIndex].position.y,
                        objectDatas[terrainObjectIndex].position.z + position.y),
                    Quaternion.Euler(objectDatas[terrainObjectIndex].rotation));
                newObject.transform.localScale = objectDatas[terrainObjectIndex].scale;
                newObject.name = "Tree "+ position.x+":"+ position.y+" nr."+ terrainObjectIndex;

                terrainObjects[terrainObjectIndex] = newObject;
                terrainObjectIndex++;

                if (terrainObjectIndex >= objectDatas.Length)
                {
                    Debug.Log("Spawn Cor Done!");
                    terrainObjectIndex = 0;
                    //isSpawningObjects = false;
                    //isDoneSpawningObjects = true;
                    yield break;
                }
                yield return null;
            }
        }


        public IEnumerator DestroyGameObjects()
        {
            const int pauseValue = 10;
            int pauseindex = 0;

            if (terrainObjects == null || terrainObjects.Length == 0)
            {
                terrainObjectIndex = 0;
                //isDestroyingObjects = false;
                //isDoneDestroyingObjects = true;
                yield break;
            }

            while (true)
            {

                //if (isSpawningObjects && !isSpawningObjects)
                //{
                //    yield return null;
                //}

                //isDestroyingObjects = true;
                //isDoneDestroyingObjects = false;

                Destroy(terrainObjects[terrainObjectIndex]);
                //terrainObjects[terrainObjectIndex].SetActive(false);
                terrainObjectIndex++;
                pauseindex++;

                if (terrainObjectIndex >= objectDatas.Length)
                {
                    terrainObjectIndex = 0;
                    //isDestroyingObjects = false;
                    //isDoneDestroyingObjects = true;
                    yield break;
                }

                if (pauseindex > pauseValue)
                {
                    pauseindex = 0;
                    yield return null;
                }
            }
        }


        public void InstantiateObjectDataArray()
        {
            ObjectGenerator.InstantiateObjectDataArray(objectDatas, mapGenerator, position);
        }


        public void SetVisible(bool visible)
        {
            prefab.SetActive(visible);
        }

        public bool isVisible()
        {
            return prefab.activeSelf;
        }
    }
}
