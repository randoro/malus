﻿using UnityEngine;
using System.Collections;

public class CaveGenerator : MonoBehaviour
{
    private MarchRenderer meshRenderer;
    public int[] points;

    void Start () {
        meshRenderer = new MarchRenderer();
        meshRenderer.Initialize();

        meshRenderer.points = points;
        meshRenderer.Render();
        Mesh newMesh = new Mesh();
        meshRenderer.ToMesh(newMesh);

        GetComponent<MeshFilter>().mesh = newMesh;

    }
	
	
}
