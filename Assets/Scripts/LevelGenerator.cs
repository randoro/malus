﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class LevelGenerator
{

    public static LayoutData GenerateLayoutMap(Point startLocation, LevelSettingsData levelSettingsData, int seed)
    {
        //float[,] layerArray = new float[mapWidth, mapHeight];

        //Point startLocation = new Point(DungeonGenerator.dungeonMapSize / 2, DungeonGenerator.dungeonMapSize /2);

        Point[] presets = {new Point(10, 5), new Point(7, 15), new Point(20, 20), new Point(4, 6), new Point(13, 5)};

        Agent agent = new Agent(startLocation, DungeonGenerator.dungeonMapSize, DungeonGenerator.dungeonMapSize, presets,
            seed, levelSettingsData);
        agent.WalkAgent();

        for (int i = 0; i < agent.roomArray.Length; i++)
        {
            if (agent.roomArray[i] != null)
            {
                FindRoomOpenings(agent.roomArray[i], agent.layerArray, i);
            }
        }


        return new LayoutData(agent.layerArray, agent.roomArray);
    }

    private static void FindRoomOpenings(Room room, byte[,] layerArray, int roomIndex)
    {
        Point[] openings = new Point[100];
        int index = 0;



        for (int i = 0; i < room.size.x; i++)
        {
            if (room.location.y - 1 < 0)
            {
                break;
            }

            if (layerArray[room.location.x + i, room.location.y - 1] == 128)
            {
                openings[index] = new Point(room.location.x + i, room.location.y);
                index++;
            }
        }

        for (int i = 0; i < room.size.y; i++)
        {
            if (room.location.x - 1 < 0)
            {
                break;
            }

            if (layerArray[room.location.x - 1, room.location.y + i] == 128)
            {
                openings[index] = new Point(room.location.x, room.location.y + i);
                index++;
            }
        }


        for (int i = 0; i < room.size.x; i++)
        {
            if (room.location.y + room.size.y >= layerArray.GetLength(1))
            {
                break;
            }

            if (layerArray[room.location.x + i, room.location.y + room.size.y] == 128)
            {
                openings[index] = new Point(room.location.x + i, room.location.y + (room.size.y - 1));
                index++;
            }
        }

        for (int i = 0; i < room.size.y; i++)
        {
            if (room.location.x + room.size.x >= layerArray.GetLength(0))
            {
                break;
            }

            if (layerArray[room.location.x + room.size.x, room.location.y + i] == 128)
            {
                openings[index] = new Point(room.location.x + (room.size.x - 1), room.location.y + i);
                index++;
            }
        }


        if (index == 0)
        {
            Debug.Log("Room doesnt have any entrance.");
        }
        else if (index == 1)
        {
            if (roomIndex == 0)
            {
                Debug.Log("Found StartRoom with one corridor.");
                room.roomType = RoomType.EntryRoom;
            }
            else
            {
                Debug.Log("Found LootRoom with one corridor.");
                room.roomType = RoomType.LootRoom;
            }

        }
        else if (index == 2)
        {
            room.roomType = RoomType.NormalRoom;
        }
        else if (index > 2)
        {
            Debug.Log("Too many Corridors in room "+roomIndex);
            Debug.Log("Corridors found:" + index);

            Debug.Log("RoomLoc: " + room.location.x + " "+ room.location.y);

            Debug.Log("RoomSize: " + room.size.x + " " + room.size.y);

            for (int i = 0; i < index; i++)
            {
                Debug.Log("Opening: " + i + " " + openings[i].x + " " + openings[i].y);
            }
        }

        room.corridorConnections = openings;
        room.corridorConnectionsFound = index;


    }
}
