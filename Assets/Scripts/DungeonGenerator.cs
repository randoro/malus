﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class DungeonGenerator : MonoBehaviour
{
    public enum DrawMode { OneLayer, FiveLayers }

    public DrawMode drawMode;
    
    [HideInInspector] public Texture2D previewLevel;

    public int seed;
    
    public LevelData[] levelData;
    

    public const int dungeonMapSize = 1000;

    Queue<DungeonThreadInfo<DungeonData>> dungeonDataThreadInfoQueue = new Queue<DungeonThreadInfo<DungeonData>>();


    void OnValuesUpdated()
    {
        if (!Application.isPlaying)
        {
            DrawDungeonInEditor();
        }
    }

    void OnValidate()
    {
        if (levelData.Length > 0)
        {
            for (int i = 0; i < levelData.Length; i++)
            {
                if (levelData[i] != null)
                {
                    levelData[i].OnValuesUpdated -= OnValuesUpdated;
                    levelData[i].OnValuesUpdated += OnValuesUpdated;

                    if (levelData[i].levelSettingsData != null)
                    {
                        levelData[i].levelSettingsData.OnValuesUpdated -= OnValuesUpdated;
                        levelData[i].levelSettingsData.OnValuesUpdated += OnValuesUpdated;
                    }
                }
            }

        }
    }


    public void DrawDungeonInEditor()
    {
        if (drawMode == DrawMode.OneLayer)
        {
            DungeonData dungeonData = GenerateDungeonData(new Point(dungeonMapSize/2, dungeonMapSize/2), 0);

            Texture2D texture = TextureGenerator.TextureFromByteMap(dungeonData.roomMap);
            previewLevel = texture;

            GameObject previewPlane = GameObject.FindGameObjectWithTag("previewPlane");
            Renderer rend = previewPlane.GetComponent<Renderer>();
            rend.sharedMaterial.mainTexture = texture;

            CorridorGenerator.InstansiateCorridors(dungeonData, levelData[0], gameObject);

            RoomGenerator.InstansiateRooms(dungeonData, levelData[0], gameObject);

            
        }
        else if (drawMode == DrawMode.FiveLayers)
        {

        }





    }

    public void RequestDungeonData(Point startLocation, int levelIndex, Action<DungeonData> callback)
    {
        ThreadStart threadStart = delegate
        {
            DungeonDataThread(startLocation, levelIndex, callback);
        };

        new Thread(threadStart).Start();
    }

    void DungeonDataThread(Point startLocation, int levelIndex, Action<DungeonData> callback)
    {
        DungeonData dungeonData = GenerateDungeonData(startLocation, levelIndex);
        lock (dungeonDataThreadInfoQueue)
        {
            dungeonDataThreadInfoQueue.Enqueue(new DungeonThreadInfo<DungeonData>(callback, dungeonData));
        }
    }


    DungeonData GenerateDungeonData(Point startLocation, int levelIndex)
    {
        LayoutData layoutData = LevelGenerator.GenerateLayoutMap(startLocation, levelData[levelIndex].levelSettingsData,
            seed + levelData[levelIndex].levelSeed);

        //generate room specific data and store in dungeondata

        return new DungeonData(layoutData.roomMap, layoutData.roomArray);
    }


    struct DungeonThreadInfo<T>
    {
        public readonly Action<T> callback;
        public readonly T parameter;

        public DungeonThreadInfo(Action<T> callback, T parameter)
        {
            this.callback = callback;
            this.parameter = parameter;
        }
    }

}

public struct DungeonData
{
    public readonly byte[,] roomMap;
    public readonly Room[] roomArray;

    public DungeonData(byte[,] roomMap, Room[] roomArray)
    {
        this.roomMap = roomMap;
        this.roomArray = roomArray;
    }
}


public struct LayoutData
{
    public readonly byte[,] roomMap;
    public readonly Room[] roomArray;

    public LayoutData(byte[,] roomMap, Room[] roomArray)
    {
        this.roomMap = roomMap;
        this.roomArray = roomArray;
    }
}