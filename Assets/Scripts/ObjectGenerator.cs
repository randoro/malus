﻿using UnityEngine;
using System.Collections;

public static class ObjectGenerator
{
    
    //Called in thread
    public static ObjectData[] GenerateObjectDataArray(float[,] heightMap, BiomeData[] biomeDatas, float[][,] strengthMap,
        float[][,] extraStrengthMaps, Vector3[,] normalMap, int seed)
    {

        int differentObjectsCount = 0;

        for (int i = 0; i < biomeDatas.Length; i++)
        {
            if (biomeDatas[i].enableForGenerator)
            {
                if (biomeDatas[i].objects != null)
                {
                    differentObjectsCount += biomeDatas[i].objects.Length;
                }
            }
        }

        AnimationCurve[] heightCurves = new AnimationCurve[differentObjectsCount];
        int curveIndex = 0;
        for (int i = 0; i < biomeDatas.Length; i++)
        {
            if (biomeDatas[i].enableForGenerator)
            {
                if (biomeDatas[i].objects != null)
                {
                    int objectsLength = biomeDatas[i].objects.Length;
                    for (int j = 0; j < objectsLength; j++)
                    {
                        if (biomeDatas[i].objects[j].splatRule != null)
                        {
                            if (biomeDatas[i].objects[j].splatRule.GetType() == typeof(SplatRuleHeight))
                            {
                                heightCurves[curveIndex] =
                                    new AnimationCurve(
                                        ((SplatRuleHeight)biomeDatas[i].objects[j].splatRule).heightStrength.keys);
                                curveIndex++;
                            }

                            if (biomeDatas[i].objects[j].splatRule.GetType() == typeof(SplatRuleAND))
                            {
                                heightCurves[curveIndex] =
                                    new AnimationCurve(
                                        ((SplatRuleAND)biomeDatas[i].objects[j].splatRule).heightRule.heightStrength.keys);
                                curveIndex++;
                            }

                        }
                    }
                }
            }
        }


        //Debug.Log("GenerateObjectDataArray");


        ObjectSet[,] objectSets =
            new ObjectSet[MapGenerator.mapActualChunkSizePlusOne, MapGenerator.mapActualChunkSizePlusOne];
        int[,] biomeIndexes = new int[MapGenerator.mapActualChunkSizePlusOne, MapGenerator.mapActualChunkSizePlusOne];
        int[,] objectIndexes = new int[MapGenerator.mapActualChunkSizePlusOne, MapGenerator.mapActualChunkSizePlusOne];
        byte[,] objectCreated = new byte[MapGenerator.mapActualChunkSizePlusOne, MapGenerator.mapActualChunkSizePlusOne];

        System.Random prng = new System.Random(seed);

        for (int y = 0; y < MapGenerator.mapActualChunkSizePlusOne; y++)
        {
            for (int x = 0; x < MapGenerator.mapActualChunkSizePlusOne; x++)
            {

                int extraStrengthIndex = 0;
                float totalChanceAtLocation = 0.0f;
                int splatHeightIndex = 0;

                for (int i = 0; i < biomeDatas.Length; i++)
                {
                    if (biomeDatas[i].enableForGenerator)
                    {
                        if (biomeDatas[i].objects != null)
                        {
                            int objectsLength = biomeDatas[i].objects.Length;
                            for (int j = 0; j < objectsLength; j++)
                            {
                                //Debug.Log("Object nr."+j);

                                if (biomeDatas[i].objects[j].textureStrength != null)
                                {
                                    totalChanceAtLocation += biomeDatas[i].objects[j].chanceOfGettingPicked*
                                                             extraStrengthMaps[extraStrengthIndex][x, y]*
                                                             strengthMap[i][x, y];
                                    extraStrengthIndex++;
                                }
                                else
                                {
                                    totalChanceAtLocation += biomeDatas[i].objects[j].chanceOfGettingPicked*
                                                             strengthMap[i][x, y];
                                }


                            }
                        }
                    }
                }

                //Debug.Log("TotalChanceAtLocation " + totalChanceAtLocation);

                float chancePick = (float) (prng.NextDouble()*totalChanceAtLocation);
                float chancePickChecker = 0.0f;
                extraStrengthIndex = 0;

                //Debug.Log("ChancePick " + chancePick);
                
                bool found = false;

                for (int i = 0; i < biomeDatas.Length; i++)
                {
                    if (found)
                    {
                        break;
                    }


                    if (biomeDatas[i].enableForGenerator)
                    {
                        if (biomeDatas[i].objects != null)
                        {
                            int objectsLength = biomeDatas[i].objects.Length;
                            for (int j = 0; j < objectsLength; j++)
                            {
                                if (found)
                                {
                                    break;
                                }


                                if (biomeDatas[i].objects[j].textureStrength != null)
                                {
                                    chancePickChecker += biomeDatas[i].objects[j].chanceOfGettingPicked*
                                                         extraStrengthMaps[extraStrengthIndex][x, y]*
                                                         strengthMap[i][x, y];
                                    extraStrengthIndex++;
                                }
                                else
                                {
                                    chancePickChecker += biomeDatas[i].objects[j].chanceOfGettingPicked*
                                                         strengthMap[i][x, y];
                                }

                                if (chancePickChecker >= chancePick)
                                {
                                    found = true;
                                    //Match on last one except for 0 index!
                                    //Debug.Log("Match found! ");

                                    ObjectSet choosenSet = biomeDatas[i].objects[j];

                                    float chance = choosenSet.concentration*strengthMap[i][x, y];

                                    if (biomeDatas[i].objects[j].splatRule != null)
                                    {
                                        if (biomeDatas[i].objects[j].splatRule.GetType() == typeof (SplatRuleHeight))
                                        {
                                            //Debug.Log("height"+height);
                                            chance *=
                                                Mathf.Clamp01(heightCurves[splatHeightIndex].Evaluate(heightMap[x, y]));
                                            splatHeightIndex++;
                                        }

                                        if (biomeDatas[i].objects[j].splatRule.GetType() == typeof(SplatRuleAND))
                                        {
                                            chance *=
                                                Mathf.Clamp01(heightCurves[splatHeightIndex].Evaluate(heightMap[x, y]) * ((SplatRuleAND)biomeDatas[i].objects[j].splatRule).normalRule.hasRightNormal(normalMap[x, y]));
                                            splatHeightIndex++;
                                        }


                                        if (biomeDatas[i].objects[j].splatRule.GetType() == typeof (SplatRuleNormal))
                                        {
                                            //Debug.Log("Splatrule Normal found");
                                            chance *=
                                                ((SplatRuleNormal) biomeDatas[i].objects[j].splatRule).hasRightNormal(
                                                    normalMap[x, y]);

                                        }
                                    }



                                    if (biomeDatas[i].objects[j].textureStrength != null)
                                    {
                                        chance *= extraStrengthMaps[extraStrengthIndex - 1][x, y];
                                    }


                                    if (chance >= prng.NextDouble())
                                    {
                                        objectSets[x, y] = choosenSet;
                                        biomeIndexes[x, y] = i;
                                        objectIndexes[x, y] = j;
                                        objectCreated[x, y] = 1;
                                        //Debug.Log("Found! ");
                                        //Debug.Log("LastBiomeIndex "+lastBiomeDataIndex);
                                        //Debug.Log("LastObjectIndex "+lastObjectDataIndex);
                                    }

                                }



                            }

                        }
                    }
                }

            }
        }

        byte[,] collisionMap = new byte[MapGenerator.mapActualChunkSizePlusOne, MapGenerator.mapActualChunkSizePlusOne];
        ObjectData[] objectDatas = new ObjectData[MapGenerator.mapActualChunkSizePlusOne * MapGenerator.mapActualChunkSizePlusOne];
        int objectDatasIndex = 0;


        //Start with full ignore
        for (int y = 0; y < MapGenerator.mapActualChunkSizePlusOne; y++)
        {
            for (int x = 0; x < MapGenerator.mapActualChunkSizePlusOne; x++)
            {
                if (objectCreated[x, y] == 1)
                {
                    if (objectSets[x, y].collisionType == CollisionType.ignoresButLeavesMark ||
                        objectSets[x, y].collisionType == CollisionType.ignoresFull)
                    {
                        if (objectSets[x, y].collisionType == CollisionType.ignoresButLeavesMark)
                        {
                            collisionMap[x, y] = 1; //change later to more advanced collision
                        }


                        float positionPlusMinusX = (float) (prng.NextDouble()*2.0f - 1.0f);
                        float positionOffsetX = positionPlusMinusX*0.5f;

                        float positionPlusMinusZ = (float) (prng.NextDouble()*2.0f - 1.0f);
                        float positionOffsetZ = positionPlusMinusZ*0.5f;

                        Vector3 position = new Vector3(y, heightMap[x, y]*MapGenerator.mapActualHeight + objectSets[x, y].heightOffset, x) +
                                           new Vector3(positionOffsetX, 0, positionOffsetZ);

                        objectDatas[objectDatasIndex] = CreateObjectDataFromObjectSet(objectSets[x, y],
                            biomeIndexes[x, y],
                            objectIndexes[x, y], position, prng);
                        objectDatasIndex++;


                    }
                }


            }
        }

        //Now checks for collisions first
        for (int y = 0; y < MapGenerator.mapActualChunkSizePlusOne; y++)
        {
            for (int x = 0; x < MapGenerator.mapActualChunkSizePlusOne; x++)
            {
                if (objectCreated[x, y] == 1)
                {
                    if (objectSets[x, y].collisionType == CollisionType.checksAndLeavesMark)
                    {
                        if (collisionMap[x, y] != 1)
                        {

                            float positionPlusMinusX = (float) (prng.NextDouble()*2.0f - 1.0f);
                            float positionOffsetX = positionPlusMinusX*0.0f;

                            float positionPlusMinusZ = (float) (prng.NextDouble()*2.0f - 1.0f);
                            float positionOffsetZ = positionPlusMinusZ*0.0f;

                            Vector3 position = new Vector3(y, heightMap[x, y]*MapGenerator.mapActualHeight + objectSets[x, y].heightOffset, x) +
                                               new Vector3(positionOffsetX, 0, positionOffsetZ); //delete maybe?

                            objectDatas[objectDatasIndex] = CreateObjectDataFromObjectSet(objectSets[x, y],
                                biomeIndexes[x, y],
                                objectIndexes[x, y], position, prng);
                            objectDatasIndex++;


                            collisionMap[x, y] = 1; //change later to more advanced collision
                        }

                    }
                }


            }
        }

        ObjectData[] realObjectDatas = new ObjectData[objectDatasIndex];

        System.Array.Copy(objectDatas, realObjectDatas, objectDatasIndex);

        

        return realObjectDatas;



    }



    private static ObjectData CreateObjectDataFromObjectSet(ObjectSet objectSet, int biomeIndex, int objectIndex, Vector3 position, System.Random prng)
    {
        Vector3 rotation = Vector3.zero;
        if (objectSet.useRandomRotation)
        {
            rotation.y = (float) prng.NextDouble()*360.0f;
        }


        Vector3 scale = Vector3.one;

        if (objectSet.useRandomScaleOffset)
        {
            float scalePlusMinus = (float) (prng.NextDouble()*2.0f - 1.0f);
            float scaleOffset = scalePlusMinus*objectSet.randomScaleOffset;
            scale += new Vector3(scaleOffset, scaleOffset, scaleOffset);
        }

        ObjectData objectData = new ObjectData(biomeIndex, objectIndex, position, rotation, scale);
        return objectData;

    }


    public static void InstantiateObjectDataArray(ObjectData[] objectDatas, MapGenerator mapGen, Vector2 positionOffset)
    {

        //Debug.Log("Objects in generation:"+ objectDatas.Length);
        if (objectDatas != null)
        {
            for (int i = 0; i < objectDatas.Length; i++)
            {
                if (mapGen.biomeData[objectDatas[i].biomeDataIndex].enableForGenerator)
                {
                    GameObject newObject = (GameObject) GameObject.Instantiate(
                        mapGen.biomeData[objectDatas[i].biomeDataIndex].objects[objectDatas[i].objectDataIndex]
                            .prefabReference,
                        new Vector3(objectDatas[i].position.x + positionOffset.x, objectDatas[i].position.y,
                            objectDatas[i].position.z + positionOffset.y), Quaternion.Euler(objectDatas[i].rotation));
                    newObject.transform.localScale = objectDatas[i].scale;
                    newObject.transform.SetParent(mapGen.transform, true);
                }
            }
        }
    }



}
