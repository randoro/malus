﻿using UnityEngine;
using System.Collections;

public static class Noise {

    public enum NormalizeMode { Local, Global }

    public static float[,] GenerateNoiseMap(int mapWidth, int mapHeight, int seed, float scale, int octaves, float persistance, float lacunarity, Vector2 offset, NormalizeMode normalizeMode, float[] inMultiplierCurve, float intensity)
    {
        //AnimationCurve multiplierCurve = new AnimationCurve(inMultiplierCurve.keys);
        float[,] noiseMap = new float[mapWidth,mapHeight];

        System.Random prng = new System.Random(seed);
        Vector2[] octaveOffsets = new Vector2[octaves];

        float maxPossibleHeight = 0;
        float amplitude = 1;
        float frequency = 1;

        for (int i = 0; i < octaves; i++)
        {
            float offsetX = prng.Next(-100000, 100000) + offset.x;
            float offsetY = prng.Next(-100000, 100000) + offset.y;
            octaveOffsets[i] = new Vector2(offsetX, offsetY);

            maxPossibleHeight += amplitude;
            amplitude *= persistance;
        }

        if (scale <= 0)
        {
            scale = 0.0001f;
        }

        float maxLocalNoiseHeight = float.MinValue;
        float minLocalNoiseHeight = float.MaxValue;

        float halfWidth = mapWidth/2f;
        float halfHeight = mapHeight / 2f;

        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                amplitude = 1;
                frequency = 1;
                float noiseHeight = 0;


                for (int i = 0; i < octaves; i++)
                {
                    float sampleX = (x-halfWidth + octaveOffsets[i].x) /scale * frequency;
                    float sampleY = (y- halfHeight + octaveOffsets[i].y) /scale * frequency;

                    float perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
                    noiseHeight += perlinValue*amplitude;

                    amplitude *= persistance;
                    frequency *= lacunarity;
                }

                //noiseHeight = Mathf.Pow(noiseHeight, 3.00f);

                if (noiseHeight > maxLocalNoiseHeight)
                {
                    maxLocalNoiseHeight = noiseHeight;
                }
                else if (noiseHeight < minLocalNoiseHeight)
                {
                    minLocalNoiseHeight = noiseHeight;
                }
                noiseMap[y, x] = noiseHeight;
            }
        }

        for (int y = 0; y < mapHeight; y++)
        {
            for (int x = 0; x < mapWidth; x++)
            {
                if (normalizeMode == NormalizeMode.Local)
                {
                    noiseMap[y, x] = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, noiseMap[y, x]) * intensity;
                }
                else
                {
                    //float normalizedHeight = (maxPossibleHeight * multiplierCurve.Evaluate(noiseMap[y, x])) /maxPossibleHeight;
                    //float normalizedHeight = (noiseMap[y, x] + 0.5f) /maxPossibleHeight;
                    //float normalizedHeight = (noiseMap[y, x] + 0.5f) / maxPossibleHeight;
                    int normIndex = (int) (Mathf.Clamp01(noiseMap[y, x]) * (MapGenerator.mapActualHeight - 1));
                    float curvedHeight = inMultiplierCurve[normIndex] * intensity;
                    noiseMap[y, x] = Mathf.Clamp01(curvedHeight); //, 0, int.MaxValue);
                }
            }
        }

        return noiseMap;
    }
}
