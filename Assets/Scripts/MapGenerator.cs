﻿using UnityEngine;
using System.Collections;
using System;
using System.Threading;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class MapGenerator : MonoBehaviour
{

    public enum DrawMode { TerrainFull, TerrainHeights, TerrainSplat, TerrainFullObjects, ExpressiveRange }

    public bool useNormalMaps;

    public DrawMode drawMode;

    public int seed;
    public BiomeData[] biomeData;

    public ClimateData[] climateData;

    public Material terrainMaterial;
    
    public bool autoUpdate;


    public const int mapActualHeight = 100;

    public const int mapActualChunkSize = 256;
    public const int mapActualChunkSizePlusOne = mapActualChunkSize + 1;
    public const int mapChunkSizeWithNormals = mapActualChunkSize + 2;
    public const int mapChunkSizeWithNormalsPlusOne = mapChunkSizeWithNormals + 1;

    [HideInInspector]
    public Texture2D expressiveRangeTexture;


    Queue<MapThreadInfo<MapData>> mapDataThreadInfoQueue = new Queue<MapThreadInfo<MapData>>();
    Queue<MapThreadInfo<SplatData>> splatDataThreadInfoQueue = new Queue<MapThreadInfo<SplatData>>();
    Queue<MapThreadInfo<ObjectData[]>> objectDatasThreadInfoQueue = new Queue<MapThreadInfo<ObjectData[]>>();
    
    void OnValuesUpdated()
    {
        if (!Application.isPlaying)
        {
            DrawMapInEditor();
        }
    }


    public void DeleteChildsInEditor()
    {
        for (int i = transform.childCount; i-- > 0;)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }
    }
    
    
    public void DrawMapInEditor()
    {

        DeleteChildsInEditor();

        EndlessTerrain endlessTerrain = FindObjectOfType<EndlessTerrain>();
        

        if (drawMode == DrawMode.TerrainFull)
        {

            endlessTerrain.prefabs[0].SetActive(false);
            endlessTerrain.prefabs[1].SetActive(false);
            endlessTerrain.prefabs[2].SetActive(false);
            endlessTerrain.prefabs[3].SetActive(false);
            endlessTerrain.prefabs[4].SetActive(true);
            endlessTerrain.prefabs[5].SetActive(false);
            endlessTerrain.prefabs[6].SetActive(false);
            endlessTerrain.prefabs[7].SetActive(false);
            endlessTerrain.prefabs[8].SetActive(false);

            MapData mapData = GenerateMapData(Vector2.zero);

            TerrainGenerator.CreateNewTerrainData(ref endlessTerrain.prefabs[4], this);
            TerrainGenerator.GenerateTerrainData(ref endlessTerrain.prefabs[4],
                mapData.heightMap, this);

            TerrainGenerator.GenerateTerrainSplat(ref endlessTerrain.prefabs[4], biomeData, SplatGenerator.GenerateSplatMap(mapData.heightMap, biomeData, mapData.strengthMap, mapData.extraTextureStrengthMaps, mapData.normalMap), this);
            
        }
        else if (drawMode == DrawMode.TerrainHeights)
        {
            endlessTerrain.prefabs[0].SetActive(false);
            endlessTerrain.prefabs[1].SetActive(false);
            endlessTerrain.prefabs[2].SetActive(false);
            endlessTerrain.prefabs[3].SetActive(false);
            endlessTerrain.prefabs[4].SetActive(true);
            endlessTerrain.prefabs[5].SetActive(false);
            endlessTerrain.prefabs[6].SetActive(false);
            endlessTerrain.prefabs[7].SetActive(false);
            endlessTerrain.prefabs[8].SetActive(false);

            MapData mapData = GenerateMapData(Vector2.zero);
            TerrainGenerator.CreateNewTerrainData(ref endlessTerrain.prefabs[4], this);
            TerrainGenerator.GenerateTerrainData(ref endlessTerrain.prefabs[4],
                mapData.heightMap, this);
        }
        else if (drawMode == DrawMode.TerrainSplat)
        {
            endlessTerrain.prefabs[0].SetActive(false);
            endlessTerrain.prefabs[1].SetActive(false);
            endlessTerrain.prefabs[2].SetActive(false);
            endlessTerrain.prefabs[3].SetActive(false);
            endlessTerrain.prefabs[4].SetActive(true);
            endlessTerrain.prefabs[5].SetActive(false);
            endlessTerrain.prefabs[6].SetActive(false);
            endlessTerrain.prefabs[7].SetActive(false);
            endlessTerrain.prefabs[8].SetActive(false);

            MapData mapData = GenerateMapData(Vector2.zero);
            TerrainGenerator.CreateNewTerrainData(ref endlessTerrain.prefabs[4], this);
            TerrainGenerator.GenerateTerrainSplat(ref endlessTerrain.prefabs[4], biomeData, SplatGenerator.GenerateSplatMap(mapData.heightMap, biomeData, mapData.strengthMap, mapData.extraTextureStrengthMaps, mapData.normalMap), this);

        }
        else if (drawMode == DrawMode.TerrainFullObjects)
        {
            endlessTerrain.prefabs[0].SetActive(false);
            endlessTerrain.prefabs[1].SetActive(false);
            endlessTerrain.prefabs[2].SetActive(false);
            endlessTerrain.prefabs[3].SetActive(false);
            endlessTerrain.prefabs[4].SetActive(true);
            endlessTerrain.prefabs[5].SetActive(false);
            endlessTerrain.prefabs[6].SetActive(false);
            endlessTerrain.prefabs[7].SetActive(false);
            endlessTerrain.prefabs[8].SetActive(false);

            MapData mapData = GenerateMapData(Vector2.zero);
            
            TerrainGenerator.CreateNewTerrainData(ref endlessTerrain.prefabs[4], this);
            TerrainGenerator.GenerateTerrainData(ref endlessTerrain.prefabs[4],
                mapData.heightMap, this);

            TerrainGenerator.GenerateTerrainSplat(ref endlessTerrain.prefabs[4], biomeData, SplatGenerator.GenerateSplatMap(mapData.heightMap, biomeData, mapData.strengthMap, mapData.extraTextureStrengthMaps, mapData.normalMap), this);

            ObjectGenerator.InstantiateObjectDataArray(ObjectGenerator.GenerateObjectDataArray(mapData.heightMap, biomeData, mapData.strengthMap, mapData.extraObjectStrengthMaps, mapData.normalMap, seed), this, Vector3.zero);



        }
        else if (drawMode == DrawMode.ExpressiveRange)
        {
            endlessTerrain.prefabs[0].SetActive(false);
            endlessTerrain.prefabs[1].SetActive(false);
            endlessTerrain.prefabs[2].SetActive(false);
            endlessTerrain.prefabs[3].SetActive(false);
            endlessTerrain.prefabs[4].SetActive(true);
            endlessTerrain.prefabs[5].SetActive(false);
            endlessTerrain.prefabs[6].SetActive(false);
            endlessTerrain.prefabs[7].SetActive(false);
            endlessTerrain.prefabs[8].SetActive(false);

            int iterations = 10000;


            const float startObjectIntensity = 0;
            const float startLinearity = 0;

            const float startObjectIntensityIncreaser = 0.01f;
            const float startLinearityIncreaser = 0.01f;

            int[,] textureArray = new int[100,100];



            for (int i = 0; i < iterations; i++)
            {
                float[] result = ExpressiveRangeGeneration(i, endlessTerrain);

                int objectIndex = (int)((result[0] - startObjectIntensity)/startObjectIntensityIncreaser);
                int linearityIndex = (int)((result[1] - startLinearity) / startLinearityIncreaser);

                if (objectIndex >= 0 && objectIndex < 100 && linearityIndex >= 0 && linearityIndex < 100)
                {
                    textureArray[linearityIndex, objectIndex] += 1;
                }


            }


            expressiveRangeTexture = TextureGenerator.ExpressiveRangeTextureFromIntMap(textureArray);






        }
    }


    private float[] ExpressiveRangeGeneration(int seed, EndlessTerrain endlessTerrain)
    {
        this.seed = seed;
        MapData mapData = GenerateMapData(Vector2.zero);
        

        TerrainGenerator.CreateNewTerrainData(ref endlessTerrain.prefabs[4], this);
        TerrainGenerator.GenerateTerrainData(ref endlessTerrain.prefabs[4],
            mapData.heightMap, this);

        TerrainGenerator.GenerateTerrainSplat(ref endlessTerrain.prefabs[4], biomeData, SplatGenerator.GenerateSplatMap(mapData.heightMap, biomeData, mapData.strengthMap, mapData.extraTextureStrengthMaps, mapData.normalMap), this);

        ObjectData[] objects = ObjectGenerator.GenerateObjectDataArray(mapData.heightMap, biomeData, mapData.strengthMap, mapData.extraObjectStrengthMaps, mapData.normalMap, seed);

        float maximumObjects = mapActualChunkSize*mapActualChunkSize*0.01f;

        float ObjectIntensity = objects.Length/maximumObjects;
        //Debug.Log("ObjectIntensity:" + ObjectIntensity);

        float wantedHeight = 0.3f;
        float linearityValue = 0;

        for (int i = 0; i < mapActualChunkSizePlusOne; i++)
        {
            for (int j = 0; j < mapActualChunkSizePlusOne; j++)
            {
                float height = mapData.heightMap[j, i];

                linearityValue += Mathf.Abs(wantedHeight - height);

            }
        }

        linearityValue = linearityValue/(mapActualChunkSizePlusOne*mapActualChunkSizePlusOne);

        //Debug.Log("LinearityValue:" + linearityValue);



        float[] returnFloat = { ObjectIntensity, linearityValue };
        return returnFloat;
    }

    public void RequestMapData(Vector2 center, Action<MapData> callback)
    {
        ThreadStart threadStart = delegate
        {
            MapDataThread(center, callback);
        };

        new Thread(threadStart).Start();
    }

    void MapDataThread(Vector2 center, Action<MapData> callback)
    {
        MapData mapData = GenerateMapData(center);
        lock (mapDataThreadInfoQueue)
        {
            mapDataThreadInfoQueue.Enqueue(new MapThreadInfo<MapData>(callback, mapData));
        }
    }

    public void RequestSplatData(MapData mapData, Action<SplatData> callback)
    {
        ThreadStart threadStart = delegate
        {
            SplatDataThread(mapData, callback);
        };

        new Thread(threadStart).Start();
    }

    void SplatDataThread(MapData mapData, Action<SplatData> callback)
    {
        SplatData splatData = new SplatData(SplatGenerator.GenerateSplatMap(mapData.heightMap, biomeData, mapData.strengthMap, mapData.extraTextureStrengthMaps, mapData.normalMap));

        lock (splatDataThreadInfoQueue)
        {
            splatDataThreadInfoQueue.Enqueue(new MapThreadInfo<SplatData>(callback, splatData));
        }
    }

    public void RequestObjectDatas(MapData mapData, Action<ObjectData[]> callback)
    {
        ThreadStart threadStart = delegate
        {
            ObjectDatasThread(mapData, callback);
        };

        new Thread(threadStart).Start();
    }

    void ObjectDatasThread(MapData mapData, Action<ObjectData[]> callback)
    {
        ObjectData[] objectDatas = ObjectGenerator.GenerateObjectDataArray(mapData.heightMap, biomeData, mapData.strengthMap, mapData.extraObjectStrengthMaps, mapData.normalMap, seed);

        lock (objectDatasThreadInfoQueue)
        {
            objectDatasThreadInfoQueue.Enqueue(new MapThreadInfo<ObjectData[]>(callback, objectDatas));
        }
    }

    void Update()
    {

        if (mapDataThreadInfoQueue.Count > 0)
        {
            //for (int i = 0; i < mapDataThreadInfoQueue.Count; i++)
            //{
                MapThreadInfo<MapData> threadInfo = mapDataThreadInfoQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            //}
        }

        if (splatDataThreadInfoQueue.Count > 0)
        {
            //for (int i = 0; i < meshDataThreadInfoQueue.Count; i++)
            //{
                MapThreadInfo<SplatData> threadInfo = splatDataThreadInfoQueue.Dequeue();
                threadInfo.callback(threadInfo.parameter);
            //}
        }

        if (objectDatasThreadInfoQueue.Count > 0)
        {
            //for (int i = 0; i < meshDataThreadInfoQueue.Count; i++)
            //{
            MapThreadInfo<ObjectData[]> threadInfo = objectDatasThreadInfoQueue.Dequeue();
            threadInfo.callback(threadInfo.parameter);
            //}
        }
    }


    MapData GenerateMapData(Vector2 center)
    {
        float[][,] jaggedNoiseArray = new float[biomeData.Length][,];
        float[][,] jaggedStrengthArray = new float[biomeData.Length][,];

        float[][,] jaggedClimateArray = new float[climateData.Length][,];

        for (int i = 0; i < climateData.Length; i++)
        {
            jaggedClimateArray[i] = Noise.GenerateNoiseMap(mapChunkSizeWithNormalsPlusOne, mapChunkSizeWithNormalsPlusOne,
                    climateData[i].strengthData.noiseSeed + seed, climateData[i].strengthData.noiseScale, climateData[i].strengthData.noiseOctaves,
                    climateData[i].strengthData.noisePersistance, climateData[i].strengthData.noiseLacunarity, center + climateData[i].strengthData.noiseOffset,
                    climateData[i].strengthData.noiseNormalizeMode, climateData[i].strengthData.sampledCurve, 1f); //change
        }
        





        int extraTextureStrengths = 0;
        int extraObjectStrengths = 0;

        for (int i = 0; i < biomeData.Length; i++)
        {
            if (biomeData[i].enableForGenerator)
            {
                if (biomeData[i].textures != null)
                {
                    int textureLength = biomeData[i].textures.Length;
                    for (int j = 0; j < textureLength; j++)
                    {
                        if (biomeData[i].textures[j].textureStrength != null)
                        {
                            extraTextureStrengths++;
                        }
                    }
                }

                if (biomeData[i].objects != null)
                {
                    int objectLength = biomeData[i].objects.Length;
                    for (int j = 0; j < objectLength; j++)
                    {
                        if (biomeData[i].objects[j].textureStrength != null)
                        {
                            extraObjectStrengths++;
                        }
                    }
                }
            }
        }



        float[][,] jaggedExtraTextureStrengthArray = new float[extraTextureStrengths][,];
        float[][,] jaggedExtraObjectStrengthArray = new float[extraObjectStrengths][,];

        int textureIndex = 0;
        int objectIndex = 0;
        for (int i = 0; i < biomeData.Length; i++)
        {
            if (biomeData[i].enableForGenerator)
            {
                jaggedNoiseArray[i] = Noise.GenerateNoiseMap(mapChunkSizeWithNormalsPlusOne, mapChunkSizeWithNormalsPlusOne,
                    biomeData[i].noiseData.noiseSeed + biomeData[i].noiseSeed + seed, biomeData[i].noiseData.noiseScale, biomeData[i].noiseData.noiseOctaves,
                    biomeData[i].noiseData.noisePersistance, biomeData[i].noiseData.noiseLacunarity, center + biomeData[i].noiseData.noiseOffset,
                    biomeData[i].noiseData.noiseNormalizeMode, biomeData[i].noiseData.sampledCurve, biomeData[i].noiseIntensity * biomeData[i].overrideIntensity);
                jaggedStrengthArray[i] = Noise.GenerateNoiseMap(mapChunkSizeWithNormalsPlusOne, mapChunkSizeWithNormalsPlusOne,
                    biomeData[i].strengthData.noiseSeed + biomeData[i].strengthSeed + seed, biomeData[i].strengthData.noiseScale, biomeData[i].strengthData.noiseOctaves,
                    biomeData[i].strengthData.noisePersistance, biomeData[i].strengthData.noiseLacunarity, center + biomeData[i].strengthData.noiseOffset,
                    biomeData[i].strengthData.noiseNormalizeMode, biomeData[i].strengthData.sampledCurve, biomeData[i].strengthIntensity * biomeData[i].overrideIntensity);


                if (biomeData[i].textures != null)
                {
                    int textureLength = biomeData[i].textures.Length;
                    for (int j = 0; j < textureLength; j++)
                    {
                        if (biomeData[i].textures[j].textureStrength != null)
                        {
                            jaggedExtraTextureStrengthArray[textureIndex] =
                                Noise.GenerateNoiseMap(mapChunkSizeWithNormalsPlusOne,
                                    mapChunkSizeWithNormalsPlusOne,
                                    biomeData[i].textures[j].textureStrength.noiseSeed +
                                    biomeData[i].textures[j].strengthSeed + seed,
                                    biomeData[i].textures[j].textureStrength.noiseScale,
                                    biomeData[i].textures[j].textureStrength.noiseOctaves,
                                    biomeData[i].textures[j].textureStrength.noisePersistance,
                                    biomeData[i].textures[j].textureStrength.noiseLacunarity,
                                    center + biomeData[i].textures[j].textureStrength.noiseOffset,
                                    biomeData[i].textures[j].textureStrength.noiseNormalizeMode,
                                    biomeData[i].textures[j].textureStrength.sampledCurve,
                                    biomeData[i].textures[j].intensity*biomeData[i].overrideIntensity);
                            textureIndex++;
                        }
                    }
                }


                if (biomeData[i].objects != null)
                {
                    int objectLength = biomeData[i].objects.Length;
                    for (int j = 0; j < objectLength; j++)
                    {
                        if (biomeData[i].objects[j].textureStrength != null)
                        {
                            jaggedExtraObjectStrengthArray[objectIndex] =
                                Noise.GenerateNoiseMap(mapChunkSizeWithNormalsPlusOne,
                                    mapChunkSizeWithNormalsPlusOne,
                                    biomeData[i].objects[j].textureStrength.noiseSeed +
                                    biomeData[i].objects[j].strengthSeed + seed,
                                    biomeData[i].objects[j].textureStrength.noiseScale,
                                    biomeData[i].objects[j].textureStrength.noiseOctaves,
                                    biomeData[i].objects[j].textureStrength.noisePersistance,
                                    biomeData[i].objects[j].textureStrength.noiseLacunarity,
                                    center + biomeData[i].objects[j].textureStrength.noiseOffset,
                                    biomeData[i].objects[j].textureStrength.noiseNormalizeMode,
                                    biomeData[i].objects[j].textureStrength.sampledCurve,
                                    biomeData[i].objects[j].intensity*biomeData[i].overrideIntensity);
                            objectIndex++;
                        }
                    }
                }

            }
            else
            {
                jaggedNoiseArray[i] = new float[mapChunkSizeWithNormalsPlusOne, mapChunkSizeWithNormalsPlusOne];
                jaggedStrengthArray[i] = new float[mapChunkSizeWithNormalsPlusOne, mapChunkSizeWithNormalsPlusOne];
            }
        }

        float[,] noiseMapWithNormals = new float[mapChunkSizeWithNormalsPlusOne, mapChunkSizeWithNormalsPlusOne];
        
        for (int y = 0; y < mapChunkSizeWithNormalsPlusOne; y++)
        {
            for (int x = 0; x < mapChunkSizeWithNormalsPlusOne; x++)
            {
                float averageDivider = 0;
                float nearestDistance = 2;
                float secondnearestDistance = 3;
                int nearestIndex = -1;
                int secondNearestIndex = -2;

                //for (int i = 0; i < biomeData.Length; i++)
                //{
                //    if (biomeData[i].enableForGenerator)
                //    {
                //        float testDistance = 0;

                //        for (int j = 0; j < jaggedClimateArray.GetLength(0); j++)
                //        {
                //            testDistance +=
                //                Math.Abs(jaggedClimateArray[j][y, x] - biomeData[i].ClimateSet[j].climateIndicator);
                //        }

                //        if (testDistance <= nearestDistance)
                //        {
                //            secondnearestDistance = nearestDistance;
                //            secondNearestIndex = nearestIndex;

                //            nearestDistance = testDistance;
                //            nearestIndex = i;

                //        }
                //        else if (testDistance <= secondnearestDistance)
                //        {
                //            secondnearestDistance = testDistance;
                //            secondNearestIndex = i;
                //        }
                //    }
                //}

                float nearestValue = 0;
                float secondNearestValue = 0;
                float actualValue = 0;

                //for (int j = 0; j < jaggedClimateArray.GetLength(0); j++)
                //{
                //    nearestValue += biomeData[nearestIndex].ClimateSet[j].climateIndicator;
                //    secondNearestValue += biomeData[nearestIndex].ClimateSet[j].climateIndicator;
                //    actualValue += Math.Abs(jaggedClimateArray[j][y, x]);
                //}

                //noiseMapWithNormals[y, x] = 0;

                ////if (actualValue <= nearestValue && secondNearestValue > nearestValue)
                ////{
                ////    //Left end
                ////    noiseMapWithNormals[y, x] = jaggedNoiseArray[nearestIndex][y, x];

                ////}

                //if (actualValue > nearestValue && secondNearestValue < nearestValue || actualValue <= nearestValue && secondNearestValue > nearestValue)
                //{
                //    //Right end
                //    noiseMapWithNormals[y, x] = jaggedNoiseArray[nearestIndex][y, x];
                //}
                //else
                //{
                //    //Middle
                //    float percent = (actualValue - nearestValue) / (secondNearestValue - nearestValue);
                //    noiseMapWithNormals[y, x] = Mathf.Clamp01(jaggedNoiseArray[nearestIndex][y, x] * (1f - percent) + jaggedNoiseArray[secondNearestIndex][y, x] * percent);
                //    //noiseMapWithNormals[y, x] += Mathf.Clamp01(jaggedNoiseArray[secondNearestIndex][y, x] * percent);
                //}





                for (int i = 0; i < biomeData.Length; i++)
                {
                    if (biomeData[i].enableForGenerator)
                    {


                        noiseMapWithNormals[y, x] +=
                            Mathf.Clamp01(jaggedNoiseArray[i][y, x] * jaggedStrengthArray[i][y, x]);
                        averageDivider += jaggedStrengthArray[i][y, x];
                    }

                }

                if (averageDivider <= 0)
                {
                    averageDivider = 1;
                }

                noiseMapWithNormals[y, x] /= averageDivider;
                noiseMapWithNormals[y, x] = Mathf.Clamp01(noiseMapWithNormals[y, x]);

            }
        }

        float[,] noiseMap = new float[mapActualChunkSizePlusOne, mapActualChunkSizePlusOne];
        Vector3[,] normalMap = new Vector3[mapActualChunkSizePlusOne, mapActualChunkSizePlusOne];

        for (int y = 0; y < mapChunkSizeWithNormalsPlusOne; y++)
        {
            for (int x = 0; x < mapChunkSizeWithNormalsPlusOne; x++)
            {
                if (x > 0 && x < mapChunkSizeWithNormalsPlusOne - 1 && y > 0 && y < mapChunkSizeWithNormalsPlusOne - 1)
                {
                    noiseMap[y - 1, x - 1] = noiseMapWithNormals[y, x];

                    
                    float hL = noiseMapWithNormals[y, x - 1];
                    float hR = noiseMapWithNormals[y, x + 1];
                    float hD = noiseMapWithNormals[y - 1, x];
                    float hU = noiseMapWithNormals[y + 1, x];
                    
                    normalMap[y - 1, x - 1] = new Vector3(hL - hR, 2.0f, hD - hU).normalized;

                    //if (normalMap[y - 1, x - 1].x < 0.1f && normalMap[y - 1, x - 1].x > -0.1f && normalMap[y - 1, x - 1].y == 1.0f &&
                    //    normalMap[y - 1, x - 1].z < 0.1f && normalMap[y - 1, x - 1].z > -0.1f)
                    //{
                    //    int yvalue = y - 1;
                    //    int xvalue = x - 1;

                        //noiseMap[y - 1, x - 1] = 1;
                        //Debug.Log("Normal at y:" + yvalue + " x:" + xvalue + " is straight up!");
                        //GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                        //cube.transform.position = new Vector3(xvalue, noiseMapWithNormals[y, x]*100, yvalue);
                        //Gizmos.DrawCube(new Vector3(xvalue, noiseMapWithNormals[y, x] * 100, yvalue), new Vector3(1, 1, 1));
                        //Instantiate(PrimitiveType.Cube, new Vector3(xvalue, noiseMapWithNormals[y, x] * 100, yvalue), Quaternion.identity)
                    //}
                    //N.x = hL - hR;
                    //N.y = 2.0;
                    //N.z = hD - hU;
                    //N = normalize(N);
                }
            }
        }



        return new MapData(noiseMap, jaggedStrengthArray, jaggedExtraTextureStrengthArray, jaggedExtraObjectStrengthArray, normalMap);
    }

    
    void OnValidate()
    {
        if (biomeData.Length > 0)
        {
            for (int i = 0; i < biomeData.Length; i++)
            {
                if (biomeData[i] != null)
                {
                    biomeData[i].OnValuesUpdated -= OnValuesUpdated;
                    biomeData[i].OnValuesUpdated += OnValuesUpdated;

                    if (biomeData[i].noiseData != null)
                    {
                        biomeData[i].noiseData.OnValuesUpdated -= OnValuesUpdated;
                        biomeData[i].noiseData.OnValuesUpdated += OnValuesUpdated;
                    }

                    if (biomeData[i].strengthData != null)
                    {
                        biomeData[i].strengthData.OnValuesUpdated -= OnValuesUpdated;
                        biomeData[i].strengthData.OnValuesUpdated += OnValuesUpdated;
                    }


                    if (biomeData[i].textures != null && biomeData[i].textures.Length > 0)
                    {
                        for (int j = 0; j < biomeData[i].textures.Length; j++)
                        {
                            if (biomeData[i].textures[j].splatRule.GetType() == typeof (SplatRuleAND))
                            {
                                biomeData[i].textures[j].splatRule.OnValuesUpdated -= OnValuesUpdated;
                                biomeData[i].textures[j].splatRule.OnValuesUpdated += OnValuesUpdated;

                                ((SplatRuleAND)biomeData[i].textures[j].splatRule).heightRule.OnValuesUpdated -= OnValuesUpdated;
                                ((SplatRuleAND)biomeData[i].textures[j].splatRule).heightRule.OnValuesUpdated += OnValuesUpdated;

                                ((SplatRuleAND)biomeData[i].textures[j].splatRule).normalRule.OnValuesUpdated -= OnValuesUpdated;
                                ((SplatRuleAND)biomeData[i].textures[j].splatRule).normalRule.OnValuesUpdated += OnValuesUpdated;
                            }
                            else
                            {
                                biomeData[i].textures[j].splatRule.OnValuesUpdated -= OnValuesUpdated;
                                biomeData[i].textures[j].splatRule.OnValuesUpdated += OnValuesUpdated;
                            }
                        }
                    }

                    if (biomeData[i].objects != null && biomeData[i].objects.Length > 0)
                    {
                        for (int j = 0; j < biomeData[i].objects.Length; j++)
                        {
                            if (biomeData[i].objects[j].splatRule.GetType() == typeof(SplatRuleAND))
                            {
                                biomeData[i].objects[j].splatRule.OnValuesUpdated -= OnValuesUpdated;
                                biomeData[i].objects[j].splatRule.OnValuesUpdated += OnValuesUpdated;

                                ((SplatRuleAND)biomeData[i].objects[j].splatRule).heightRule.OnValuesUpdated -= OnValuesUpdated;
                                ((SplatRuleAND)biomeData[i].objects[j].splatRule).heightRule.OnValuesUpdated += OnValuesUpdated;

                                ((SplatRuleAND)biomeData[i].objects[j].splatRule).normalRule.OnValuesUpdated -= OnValuesUpdated;
                                ((SplatRuleAND)biomeData[i].objects[j].splatRule).normalRule.OnValuesUpdated += OnValuesUpdated;
                            }
                            else
                            {
                                biomeData[i].objects[j].splatRule.OnValuesUpdated -= OnValuesUpdated;
                                biomeData[i].objects[j].splatRule.OnValuesUpdated += OnValuesUpdated;
                            }
                        }
                    }
                }
            }
        }
    }


    struct MapThreadInfo<T>
    {
        public readonly Action<T> callback;
        public readonly T parameter;

        public MapThreadInfo(Action<T> callback, T parameter)
        {
            this.callback = callback;
            this.parameter = parameter;
        } 
    }

}

public struct MapData
{
    public readonly float[,] heightMap;
    public readonly float[][,] strengthMap;
    public readonly float[][,] extraTextureStrengthMaps;
    public readonly float[][,] extraObjectStrengthMaps;
    public readonly Vector3[,] normalMap;

    public MapData(float[,] heightMap, float[][,] strengthMap, float[][,] extraTextureStrengthMaps, float[][,] extraObjectStrengthMaps, Vector3[,] normalMap)
    {
        this.heightMap = heightMap;
        this.strengthMap = strengthMap;
        this.extraTextureStrengthMaps = extraTextureStrengthMaps;
        this.extraObjectStrengthMaps = extraObjectStrengthMaps;
        this.normalMap = normalMap;
    }
}

public struct SplatData
{
    public readonly float[,,] splatMap;

    public SplatData(float[,,] splatMap)
    {
        this.splatMap = splatMap;
    }
}

public struct ObjectData
{
    public readonly int biomeDataIndex;
    public readonly int objectDataIndex;
    public readonly Vector3 position;
    public readonly Vector3 rotation;
    public readonly Vector3 scale;


    public ObjectData(int biomeDataIndex, int objectDataIndex, Vector3 position, Vector3 rotation, Vector3 scale)
    {
        this.biomeDataIndex = biomeDataIndex;
        this.objectDataIndex = objectDataIndex;
        this.position = position;
        this.rotation = rotation;
        this.scale = scale;

    }

}