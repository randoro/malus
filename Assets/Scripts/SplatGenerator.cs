﻿using UnityEngine;
using System.Collections;
using System.Linq; // used for Sum of array

public static class SplatGenerator {


    public static float[,,] GenerateSplatMap(float[,] heightMap, BiomeData[] biomeDatas, float[][,] strengthMap, float[][,] extraStrengthMaps, Vector3[,] normalMap)
    {
        
        int textureCount = 0;

        for (int i = 0; i < biomeDatas.Length; i++)
        {
            if (biomeDatas[i].enableForGenerator)
            {
                if (biomeDatas[i].textures != null)
                {
                    textureCount += biomeDatas[i].textures.Length;
                }
            }
        }


        AnimationCurve[] heightCurves = new AnimationCurve[textureCount];
        int curveIndex = 0;
        for (int i = 0; i < biomeDatas.Length; i++)
        {
            if (biomeDatas[i].enableForGenerator)
            {
                if (biomeDatas[i].textures != null)
                {
                    int textureLength = biomeDatas[i].textures.Length;
                    for (int j = 0; j < textureLength; j++)
                    {
                        if (biomeDatas[i].textures[j].splatRule != null)
                        {
                            if (biomeDatas[i].textures[j].splatRule.GetType() == typeof (SplatRuleHeight))
                            {
                                heightCurves[curveIndex] =
                                    new AnimationCurve(
                                        ((SplatRuleHeight) biomeDatas[i].textures[j].splatRule).heightStrength.keys);
                                //AnimationCurve newAnimationCurve = new AnimationCurve(((SplatRuleHeight)biomeDatas[i].textures[j].splatRule).heightStrength.keys);

                                curveIndex++;
                            }

                            if (biomeDatas[i].textures[j].splatRule.GetType() == typeof(SplatRuleAND))
                            {
                                heightCurves[curveIndex] =
                                    new AnimationCurve(
                                        ((SplatRuleAND)biomeDatas[i].textures[j].splatRule).heightRule.heightStrength.keys);
                                //AnimationCurve newAnimationCurve = new AnimationCurve(((SplatRuleHeight)biomeDatas[i].textures[j].splatRule).heightStrength.keys);

                                curveIndex++;
                            }
                        }
                    }
                }
            }
        }
        

        float[,,] splatmapData = new float[MapGenerator.mapActualChunkSize, MapGenerator.mapActualChunkSize, textureCount];
        
        for (int y = 0; y < MapGenerator.mapActualChunkSize; y++)
        {
            for (int x = 0; x < MapGenerator.mapActualChunkSize; x++)
            {
                // Normalise x/y coordinates to range 0-1 

                // Sample the height at this location (note GetHeight expects int coordinates corresponding to locations in the heightmap array)
                //float height = textureData.GetHeight(Mathf.RoundToInt(y_01 * textureData.heightmapHeight), Mathf.RoundToInt(x_01 * textureData.heightmapWidth));
                //float height = heightMap[Mathf.RoundToInt(x_01 * heightmapWidth), Mathf.RoundToInt(y_01 * heightmapHeight)];
                float height = heightMap[x,y];
                // Calculate the normal of the terrain (note this is in normalised coordinates relative to the overall terrain dimensions)
                //Vector3 normal = textureData.GetInterpolatedNormal(y_01, x_01);

                // Calculate the steepness of the terrain
                //float steepness = textureData.GetSteepness(y_01, x_01);

                // Setup an array to record the mix of texture weights at this point
                float[] splatWeights = new float[textureCount];
                int textureIndex = 0;
                int splatHeightIndex = 0;
                int extraStrengthIndex = 0;

                for (int i = 0; i < biomeDatas.Length; i++)
                {
                    if (biomeDatas[i].enableForGenerator)
                    {
                        if (biomeDatas[i].textures != null)
                        {
                            int textureLength = biomeDatas[i].textures.Length;
                            for (int j = 0; j < textureLength; j++)
                            {
                                if (biomeDatas[i].textures[j].splatRule != null)
                                {

                                    if (x == 0 && y == 0)
                                    {
                                        Debug.Log("height="+height);
                                    }

                                    if (biomeDatas[i].textures[j].splatRule.GetType() == typeof (SplatRuleHeight))
                                    {
                                        splatWeights[textureIndex] = Mathf.Clamp01(((SplatRuleHeight)biomeDatas[i].textures[j].splatRule).sampledCurve[(int)(height * (MapGenerator.mapActualHeight - 1))] * biomeDatas[i].textures[j].intensity);
                                        splatHeightIndex++;
                                    }

                                    if (biomeDatas[i].textures[j].splatRule.GetType() == typeof(SplatRuleAND))
                                    {
                                        splatWeights[textureIndex] =
                                            Mathf.Clamp01(((SplatRuleAND)biomeDatas[i].textures[j].splatRule).heightRule.sampledCurve[(int)(height * (MapGenerator.mapActualHeight - 1))] * ((SplatRuleAND)biomeDatas[i].textures[j].splatRule).normalRule.hasRightNormal(normalMap[x, y]) * biomeDatas[i].textures[j].intensity);
                                        splatHeightIndex++;
                                    }

                                    if (biomeDatas[i].textures[j].splatRule.GetType() == typeof (SplatRuleNormal))
                                    {
                                        //Debug.Log("Splatrule Normal found");
                                        splatWeights[textureIndex] =
                                            Mathf.Clamp01(((SplatRuleNormal) biomeDatas[i].textures[j].splatRule).hasRightNormal(
                                                normalMap[x, y]) * biomeDatas[i].textures[j].intensity);

                                    }
                                }

                                if (biomeDatas[i].textures[j].textureStrength != null)
                                {
                                    splatWeights[textureIndex] *= extraStrengthMaps[extraStrengthIndex][x, y];
                                    extraStrengthIndex++;
                                }
                                //else //without this the biome strengthmap is added together with texture strengthmap
                                //{
                                splatWeights[textureIndex] *= strengthMap[i][x, y];
                                //}

                                textureIndex++;
                            }
                        }
                    }
                }

                // CHANGE THE RULES BELOW TO SET THE WEIGHTS OF EACH TEXTURE ON WHATEVER RULES YOU WANT

                // Texture[0] has constant influence
                //splatWeights[0] = Mathf.Clamp01(heightCurve0.Evaluate(height));

                // Texture[1] is stronger at lower altitudes
                //splatWeights[1] = Mathf.Clamp01(heightCurve1.Evaluate(height)); //= Mathf.Clamp01(-height + 0.2f);

                // Texture[2] stronger on flatter terrain
                // Note "steepness" is unbounded, so we "normalise" it by dividing by the extent of heightmap height and scale factor
                // Subtract result from 1.0 to give greater weighting to flat surfaces
                //splatWeights[2] = 1.0f - Mathf.Clamp01(steepness * steepness / (textureData.heightmapHeight / 5.0f));

                //splatWeights[2] = Mathf.Clamp01(heightCurve2.Evaluate(height));//= Mathf.Clamp01(height - 0.5f);

                // Texture[3] increases with height but only on surfaces facing positive Z axis 
                //splatWeights[3] = Mathf.Clamp01(heightCurve3.Evaluate(height));//= Mathf.Clamp01(height); //normal

                // Sum of all textures weights must add to 1, so calculate normalization factor from sum of weights
                float z = splatWeights.Sum();
                if (z <= 0)
                {
                    z = 1;
                }

                // Loop through each terrain texture
                for (int i = 0; i < textureCount; i++)
                {

                    // Normalize so that sum of all texture weights = 1
                    splatWeights[i] /= z;

                    // Assign this point to the splatmap array
                    splatmapData[x, y, i] = splatWeights[i];
                }
            }
        }

        return splatmapData;
    }
}
