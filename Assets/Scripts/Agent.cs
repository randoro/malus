﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agent
{

    public enum Direction
    {
        Xdir,
        Ydir,
        MinusXdir,
        MinusYdir,
        None
    }

    public Point location;
    public byte[,] layerArray;
    public Room[] roomArray;
    public byte[,] preCorridorArray;
    public Point[] roomPresets;
    System.Random prng;
    public Direction direction;
    public int roomCount;
    public int chainCount;
    public LevelSettingsData levelSettingsData;

    public Agent(Point startLocation, int mapWidth, int mapHeight, Point[] roomPresets, int seed, LevelSettingsData levelSettingsData)
    {
        location = startLocation;
        layerArray = new byte[mapWidth, mapHeight];
        roomArray = new Room[levelSettingsData.maxRooms];
        preCorridorArray = new byte[mapWidth, mapHeight];
        this.roomPresets = roomPresets;
        this.levelSettingsData = levelSettingsData;
        prng = new System.Random(seed);
        direction = Direction.None;
        roomCount = 0;
        chainCount = 0;
    }


    public void WalkAgent()
    {
        if (TryCreateRoom())
        {
            Debug.Log(roomCount);
        }
    }

    void EmptyPreArray()
    {
        preCorridorArray = new byte[preCorridorArray.GetLength(0), preCorridorArray.GetLength(1)];
    }
    
    static bool IsRoomPlacable(Point roomLocation, Point size, byte[,] layerArray, byte[,] preCorridorArray)
    {
        if (roomLocation.x < 0 || roomLocation.y < 0 || roomLocation.x + size.x >= layerArray.GetLength(0) ||
            roomLocation.y + size.y >= layerArray.GetLength(1))
        {
            //Outside layerArray
            return false;
        }

        int previewConnections = 0;


        for (int i = 0; i < size.y; i++)
        {
            for (int j = 0; j < size.x; j++)
            {
                if (!IsTileEmpty(new Point(roomLocation.x + j, roomLocation.y + i), layerArray, 255))
                {
                    return false;
                }

                if (!IsTileEmpty(new Point(roomLocation.x + j, roomLocation.y + i), preCorridorArray, 128))
                {
                    return false;
                }

                if (!IsTileNeighborEmpty(new Point(roomLocation.x + j, roomLocation.y + i), layerArray, 255, 128))
                {
                    return false;
                }

                if (!IsTileNeighborEmpty(new Point(roomLocation.x + j, roomLocation.y + i), preCorridorArray, 128, 128))
                {
                    previewConnections++;
                }

            }
        }

        if (previewConnections > 1)
        {
            return false;
        }



        return true;
    }
    
    static bool IsTileEmpty(Point tile, byte[,] layerArray, byte falseValue)
    {
        if (tile.x < 0 || tile.y < 0 || tile.x >= layerArray.GetLength(0) ||
            tile.y >= layerArray.GetLength(1))
        {
            //Outside layerArray
            return false;
        }

        if (layerArray[tile.x, tile.y] == 255 || layerArray[tile.x, tile.y] == falseValue)
        {
            //Intersects room or corridor
            return false;
        }
        
        return true;
    }

    static bool IsTileNeighborEmpty(Point tile, byte[,] layerArray, byte falseValue1, byte falseValue2)
    {
        Point neighborTile1 = new Point(tile.x - 1, tile.y);
        Point neighborTile2 = new Point(tile.x + 1, tile.y);
        Point neighborTile3 = new Point(tile.x, tile.y - 1);
        Point neighborTile4 = new Point(tile.x, tile.y + 1);

        if (!(neighborTile1.x < 0 || neighborTile1.y < 0 || neighborTile1.x >= layerArray.GetLength(0) ||
            neighborTile1.y >= layerArray.GetLength(1)))
        {
            //Neighbor inside Array
            if (layerArray[neighborTile1.x, neighborTile1.y] == falseValue1 || layerArray[neighborTile1.x, neighborTile1.y] == falseValue2)
            {
                //Neighbor intersects room or corridor
                return false;
            }
        }

        if (!(neighborTile2.x < 0 || neighborTile2.y < 0 || neighborTile2.x >= layerArray.GetLength(0) ||
            neighborTile2.y >= layerArray.GetLength(1)))
        {
            //Neighbor inside Array
            if (layerArray[neighborTile2.x, neighborTile2.y] == falseValue1 || layerArray[neighborTile2.x, neighborTile2.y] == falseValue2)
            {
                //Neighbor intersects room or corridor
                return false;
            }
        }

        if (!(neighborTile3.x < 0 || neighborTile3.y < 0 || neighborTile3.x >= layerArray.GetLength(0) ||
            neighborTile3.y >= layerArray.GetLength(1)))
        {
            //Neighbor inside Array
            if (layerArray[neighborTile3.x, neighborTile3.y] == falseValue1 || layerArray[neighborTile3.x, neighborTile3.y] == falseValue2)
            {
                //Neighbor intersects room or corridor
                return false;
            }
        }

        if (!(neighborTile4.x < 0 || neighborTile4.y < 0 || neighborTile4.x >= layerArray.GetLength(0) ||
            neighborTile4.y >= layerArray.GetLength(1)))
        {
            //Neighbor inside Array
            if (layerArray[neighborTile4.x, neighborTile4.y] == falseValue1 || layerArray[neighborTile4.x, neighborTile4.y] == falseValue2)
            {
                //Neighbor intersects room or corridor
                return false;
            }
        }




        return true;

    }
    
    void PlaceRoom(Point roomLocation, Point roomSize)
    {
        roomArray[roomCount] = new Room(roomLocation, roomSize, RoomType.NormalRoom);


        roomCount++;
        chainCount++;


        for (int i = 0; i < roomSize.y; i++)
        {
            for (int j = 0; j < roomSize.x; j++)
            {
                layerArray[roomLocation.x + j, roomLocation.y + i] = 255;
            }
        }
    }
    
    bool TryCreateRoom()
    {
        Point roomSize = new Point(0, 0);
        Point roomOffset = new Point(0, 0);
        int roomPresetIndex = prng.Next(0, roomPresets.Length);
        bool roomPlaced = false;
        for (int i = 0; i < roomPresets.Length; i++)
        {
            int realRoomPresetIndex = (roomPresetIndex + i)%roomPresets.Length;

            roomSize = roomPresets[realRoomPresetIndex];
            switch (direction)
            {
                case Direction.Xdir:
                    roomOffset = new Point(0, prng.Next(0, roomSize.y));
                    break;
                case Direction.Ydir:
                    roomOffset = new Point(prng.Next(0, roomSize.x), 0);
                    break;
                case Direction.MinusXdir:
                    roomOffset = new Point(roomSize.x - 1, prng.Next(0, roomSize.y));
                    break;
                case Direction.MinusYdir:
                    roomOffset = new Point(prng.Next(0, roomSize.x), roomSize.y - 1);
                    break;
                case Direction.None:
                    roomOffset = new Point(prng.Next(0, roomSize.x), prng.Next(0, roomSize.y));
                    break;

            }
            

            if (IsRoomPlacable(location - roomOffset, roomSize, layerArray, preCorridorArray))
            {
                //Place Room
                PlaceRoom(location - roomOffset, roomSize);
                //Make precorridors real
                MakePreCorridorsReal();
                EmptyPreArray();
                roomPlaced = true;
                break;
            }
        }

        if (!roomPlaced)
        {
            ////CreatePreCorridor
            //for (int i = 0; i < maxTries; i++)
            //{
            //    //if (TryCreatePreCorridor())
            //    //{
            //    //    if (TryCreatePrePercentage())
            //    //    {
            //    //        break;
            //    //    }
            //    //}
            // EmptyPreArray();
            //}
        }
        else
        {
            if (roomCount < levelSettingsData.maxRooms)
            {
                if (chainCount < levelSettingsData.maxChainLength)
                {
                    //CreatePreRoomCorridor
                    Point oldLocation = location;
                    for (int i = 0; i < levelSettingsData.maxTries; i++)
                    {
                        location = oldLocation;
                        if (TryCreatePreRoomCorridor(location - roomOffset, roomSize))
                        {
                            if (TryCreatePrePercentage())
                            {
                                break;
                            }
                        }
                        EmptyPreArray();
                    }
                }
            }

        }

        return roomPlaced;

    }
    
    bool TryCreatePreRoomCorridor(Point roomLocation, Point roomSize)
    {
        Direction randomDirection = (Direction) prng.Next(0, 4);
        Point roomOffset = new Point(prng.Next(0, roomSize.x), prng.Next(0, roomSize.y));
        Point directionVector = new Point(0, 0);
        Point otherDirectionVector = new Point(0, 0);
        Point newLocation = location;

        switch (randomDirection)
        {
            case Direction.Xdir:
                newLocation.x = roomLocation.x + roomSize.x;
                newLocation.y = roomLocation.y + roomOffset.y;
                directionVector.x = 1;
                otherDirectionVector.y = 1;
                break;
            case Direction.Ydir:
                newLocation.x = roomLocation.x + roomOffset.x;
                newLocation.y = roomLocation.y + roomSize.y;
                directionVector.y = 1;
                otherDirectionVector.x = 1;
                break;
            case Direction.MinusXdir:
                newLocation.x = roomLocation.x - 1;
                newLocation.y = roomLocation.y + roomOffset.y;
                directionVector.x = -1;
                otherDirectionVector.y = -1;
                break;
            case Direction.MinusYdir:
                newLocation.x = roomLocation.x + roomOffset.x;
                newLocation.y = roomLocation.y - 1;
                directionVector.y = -1;
                otherDirectionVector.x = -1;
                break;
        }

        //Check so entrance isnt beside exit
        if (!IsTileEmpty(new Point(newLocation.x + otherDirectionVector.x, newLocation.y + otherDirectionVector.y),
                    layerArray, 128))
        {
            return false;
        }
        if (!IsTileEmpty(new Point(newLocation.x - otherDirectionVector.x, newLocation.y - otherDirectionVector.y),
                    layerArray, 128))
        {
            return false;
        }


        int corridorLength = prng.Next(5, 6);

        //Check if collides
        for (int i = 0; i < corridorLength; i++)
        {
            if (!IsTileEmpty(new Point(newLocation.x + directionVector.x*i, newLocation.y + directionVector.y*i),
                    layerArray, 255))
            {
                return false;
            }

            if (!IsTileEmpty(new Point(newLocation.x + directionVector.x * i, newLocation.y + directionVector.y * i),
                    layerArray, 128))
            {
                return false;
            }

            if (!IsTileEmpty(new Point(newLocation.x + directionVector.x * i, newLocation.y + directionVector.y * i),
                    preCorridorArray, 128))
            {
                return false;
            }

            if (i != 0)
            {
                if (!IsTileNeighborEmpty(new Point(newLocation.x + directionVector.x * i, newLocation.y + directionVector.y * i),
                    layerArray, 255, 255))
                {
                    return false;
                }
            }

        }

        //Now place
        for (int i = 0; i < corridorLength; i++)
        {
            preCorridorArray[newLocation.x + directionVector.x*i, newLocation.y + directionVector.y*i] = 128;
        }


        //Set Location to end of corridor
        location.x = newLocation.x + directionVector.x*corridorLength;
        location.y = newLocation.y + directionVector.y * corridorLength;
        direction = randomDirection;

        return true;
    }
    
    bool TryCreatePreCorridor()
    {
        Direction randomDirection = (Direction)prng.Next(0, 4);
        Point directionVector = new Point(0, 0);
        Point newLocation = location;

        switch (randomDirection)
        {
            case Direction.Xdir:
                newLocation.x = location.x;
                directionVector.x = 1;
                break;
            case Direction.Ydir:
                newLocation.y = location.y;
                directionVector.y = 1;
                break;
            case Direction.MinusXdir:
                newLocation.x = location.x;
                directionVector.x = -1;
                break;
            case Direction.MinusYdir:
                newLocation.y = location.y;
                directionVector.y = -1;
                break;
        }

        int corridorLength = prng.Next(5, 6);

        //Check if collides
        for (int i = 0; i < corridorLength; i++)
        {
            if (!IsTileEmpty(new Point(newLocation.x + directionVector.x * i, newLocation.y + directionVector.y * i),
                    layerArray, 255))
            {
                return false;
            }

            if (!IsTileEmpty(new Point(newLocation.x + directionVector.x * i, newLocation.y + directionVector.y * i),
                    layerArray, 128))
            {
                return false;
            }

            if (!IsTileEmpty(new Point(newLocation.x + directionVector.x * i, newLocation.y + directionVector.y * i),
                    preCorridorArray, 128))
            {
                return false;
            }

            if (!IsTileNeighborEmpty(new Point(newLocation.x + directionVector.x * i, newLocation.y + directionVector.y * i),
                    layerArray, 255, 128))
            {
                return false;
            }

        }

        //Now place
        for (int i = 0; i < corridorLength; i++)
        {
            preCorridorArray[newLocation.x + directionVector.x * i, newLocation.y + directionVector.y * i] = 128;
        }


        //Set Location to end of corridor
        location.x = newLocation.x + directionVector.x * corridorLength;
        location.y = newLocation.y + directionVector.y * corridorLength;
        direction = randomDirection;

        return true;
    }
    
    bool TryCreatePrePercentage()
    {
        int percentage = prng.Next(0, levelSettingsData.corridorCrawliness* levelSettingsData.corridorCrawliness);


        for (int i = 0; i < levelSettingsData.corridorCrawliness* levelSettingsData.corridorCrawliness; i++) //needs to be static compared to crawliness
        {
            if (percentage < 2)
            {
                //Create room
                return TryCreateRoom();
            }
            else
            {
                //Create Corridor
                if (TryCreatePreCorridor())
                {
                    return TryCreatePrePercentage();
                }
            }
        }

        return false;




        //for (int i = 0; i < size.y; i++)
        //{
        //    for (int j = 0; j < size.x; j++)
        //    {
        //        if (location.x + j < layerArray.GetLength(0) && location.y + i < layerArray.GetLength(1))
        //        {
        //            layerArray[location.x + j, location.y + i] = 128;
        //        }
        //    }
        //}
    }
    
    void MakePreCorridorsReal()
    {
        for (int i = 0; i < preCorridorArray.GetLength(1); i++)
        {
            for (int j = 0; j < preCorridorArray.GetLength(0); j++)
            {
                if (preCorridorArray[j, i] == 128 && layerArray[j, i] != 255)
                {
                    layerArray[j, i] = 128;
                }

            }

        }
    }
    

}

public struct Point
{
    public int x;
    public int y;

    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public static Point operator +(Point p1, Point p2)
    {
        return new Point(p1.x + p2.x, p1.y + p2.y);
    }

    public static Point operator -(Point p1, Point p2)
    {
        return new Point(p1.x - p2.x, p1.y - p2.y);
    }

}

public enum RoomType { EntryRoom, NormalRoom, LootRoom, ExitRoom }

public class Room
{
    public Point location;
    public Point size;
    public RoomType roomType;
    public Point[] corridorConnections;
    public int corridorConnectionsFound;

    public Room(Point location, Point size, RoomType roomType)
    {
        this.location = location;
        this.size = size;
        this.roomType = roomType;
    }
}
