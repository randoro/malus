﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public static class TerrainGenerator
{

    public static void GenerateTerrainData(ref GameObject TerrainObj, float[,] heightMap, MapGenerator mapGen)
    {
        if (TerrainObj != null)
        {

            Terrain terrain = TerrainObj.GetComponent<Terrain>();
            TerrainCollider terrainCollider = TerrainObj.GetComponent<TerrainCollider>();

            if (terrain.terrainData == null)
            {
                CreateNewTerrainData(ref TerrainObj, mapGen);
            }

            TerrainData terrainData = terrain.terrainData;

            terrainData.SetHeights(0, 0, heightMap);

            terrainCollider.terrainData = terrainData;
            terrain.terrainData = terrainData;
            terrain.Flush();
            
        }

    }

    public static void CreateNewTerrainData(ref GameObject TerrainObj, MapGenerator mapGen)
    {
        if (TerrainObj != null)
        {
            Terrain terrain = TerrainObj.GetComponent<Terrain>();
            TerrainCollider terrainCollider = TerrainObj.GetComponent<TerrainCollider>();
            TerrainData terrainData = new TerrainData();
            terrain.basemapDistance = 400;
            terrain.heightmapPixelError = 5;
            terrain.drawTreesAndFoliage = false;

            terrainData.heightmapResolution = MapGenerator.mapActualChunkSizePlusOne;
            terrainData.baseMapResolution = MapGenerator.mapActualChunkSize;
            terrainData.SetDetailResolution(MapGenerator.mapActualChunkSize, 16);
            terrainData.size = new Vector3(MapGenerator.mapActualChunkSize, MapGenerator.mapActualHeight, MapGenerator.mapActualChunkSize);
            terrainData.alphamapResolution = MapGenerator.mapActualChunkSize;
            
            


            terrainCollider.terrainData = terrainData;
            terrain.terrainData = terrainData;
            terrain.Flush();

            if (Application.isPlaying)
            {
                terrain.materialTemplate = new Material(mapGen.terrainMaterial);
                terrain.materialType = Terrain.MaterialType.Custom;
            }
            else
            {
                terrain.materialType = Terrain.MaterialType.BuiltInLegacyDiffuse;
            }


        }
    }


    public static void GenerateTerrainSplat(ref GameObject TerrainObj, BiomeData[] BiomeData,
        float[,,] splatData, MapGenerator mapGen)
    {
        Terrain terrain = TerrainObj.GetComponent<Terrain>();
        int totalTextureLength = splatData.GetLength(2);


        if (terrain.terrainData.splatPrototypes.Length != totalTextureLength)
        {
            SplatPrototype[] tex = new SplatPrototype[totalTextureLength];
            int textureIndex = 0;

            for (int i = 0; i < BiomeData.Length; i++)
            {
                if (BiomeData[i].enableForGenerator)
                {
                    int textureLength = BiomeData[i].textures.Length;
                    for (int j = 0; j < textureLength; j++)
                    {
                        tex[textureIndex] = new SplatPrototype();
                        tex[textureIndex].texture = BiomeData[i].textures[j].texture; //Sets the texture
                        if (mapGen.useNormalMaps)
                        {
                            if (BiomeData[i].textures[j].normalMap != null)
                            {
                                tex[textureIndex].normalMap = BiomeData[i].textures[j].normalMap;
                            }
                        }
                        tex[textureIndex].tileSize = new Vector2(BiomeData[i].textures[j].textureWidth,
                            BiomeData[i].textures[j].textureHeight); //Sets the size of the texture
                        textureIndex++;
                    }
                }
            }
            terrain.terrainData.splatPrototypes = tex;
        }
        terrain.terrainData.SetAlphamaps(0, 0, splatData);
        terrain.Flush();
    }

}