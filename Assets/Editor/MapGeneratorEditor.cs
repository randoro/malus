﻿using UnityEngine;
using System.Collections;
using UnityEditor;


[CustomEditor(typeof (MapGenerator))]
public class MapGeneratorEditor : Editor
{
    private MapGenerator data = null;

    void OnEnable()
    {
        data = (MapGenerator)target;
    }

    public override void OnInspectorGUI()
    {
        

        if (DrawDefaultInspector())
        {
            if (data.autoUpdate)
            {
                data.DrawMapInEditor();
            }
        }

        if (data.expressiveRangeTexture != null)
        {
            GUILayout.Label("Expressive Range Texture");
            GUILayout.Label(data.expressiveRangeTexture, GUILayout.MaxHeight(250), GUILayout.MaxWidth(250));
        }


        if (GUILayout.Button("Generate", GUILayout.Height(40)))
        {
            data.DrawMapInEditor();
            EditorUtility.SetDirty(target);
        }
        if (GUILayout.Button("Random & Generate", GUILayout.Height(40)))
        {
            GenerateNewBiomeSeeds();
            data.DrawMapInEditor();
            EditorUtility.SetDirty(target);
        }
        if (GUILayout.Button("Remove Objects", GUILayout.Height(40)))
        {
            data.DeleteChildsInEditor();
            EditorUtility.SetDirty(target);
        }

    }


    void GenerateNewBiomeSeeds()
    {
        data.seed = Random.Range(0, 1000000);

        for (int i = 0; i < data.biomeData.Length; i++)
        {

            data.biomeData[i].noiseSeed = Random.Range(0, 1000000);
            data.biomeData[i].strengthSeed = Random.Range(0, 1000000);

            for (int j = 0; j < data.biomeData[i].textures.Length; j++)
            {
                data.biomeData[i].textures[j].strengthSeed = Random.Range(0, 1000000);
            }
        }
    }
}
