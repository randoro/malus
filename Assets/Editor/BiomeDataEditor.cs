﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(BiomeData), true)]
public class BiomeDataEditor : Editor {
    
    private BiomeData data = null;

    void OnEnable()
    {
        data = (BiomeData)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        

        if (data.previewNoise != null)
        {
            GUILayout.Label("Noise Texture");
            GUILayout.Label(data.previewNoise, GUILayout.MaxHeight(250), GUILayout.MaxWidth(250));
        }
        if (data.previewStrength != null)
        {
            GUILayout.Label("Strength Texture");
            GUILayout.Label(data.previewStrength, GUILayout.MaxHeight(250), GUILayout.MaxWidth(250));
        }

        

        if (GUILayout.Button("Update Preview", GUILayout.Height(40)))
        {
            data.CreateEditorPreviewTextures();
        }
        

        if (GUILayout.Button("Update To Generator", GUILayout.Height(40)))
        {
            data.NotifyOfUpdatedValues();
            EditorUtility.SetDirty(target);
        }

        if (GUILayout.Button("Random & Update", GUILayout.Height(40)))
        {
            GenerateNewSeeds();
            data.CreateEditorPreviewTextures();
            data.NotifyOfUpdatedValues();
            EditorUtility.SetDirty(target);
        }

    }

    void GenerateNewSeeds()
    {
        data.noiseSeed = Random.Range(0, 1000000);
        data.strengthSeed = Random.Range(0, 1000000);

        for (int i = 0; i < data.textures.Length; i++)
        {
            data.textures[i].strengthSeed = Random.Range(0, 1000000);
        }
    }
}
