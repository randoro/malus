﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(DungeonGenerator))]
public class DungeonGeneratorEditor : Editor {

    private DungeonGenerator data = null;


    void OnEnable()
    {
        data = (DungeonGenerator)target;
    }

    public override void OnInspectorGUI()
    {


        if (DrawDefaultInspector())
        {
            //if (data.autoUpdate)
            //{
            //    data.DrawMapInEditor();
            //}
        }

        if (data.previewLevel != null)
        {
            GUILayout.Label("Level Texture");
            GUILayout.Label(data.previewLevel, GUILayout.MaxHeight(250), GUILayout.MaxWidth(250));
        }
        

        if (GUILayout.Button("Generate", GUILayout.Height(40)))
        {
            data.DrawDungeonInEditor();
            EditorUtility.SetDirty(target);
        }
        if (GUILayout.Button("Random & Generate", GUILayout.Height(40)))
        {
            GenerateNewSeed();
            data.DrawDungeonInEditor();
            EditorUtility.SetDirty(target);
        }


    }


    void GenerateNewSeed()
    {
        data.seed = Random.Range(0, 1000000);
        
    }


}
