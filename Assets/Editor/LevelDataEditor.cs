﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelData), true)]
public class LevelDataEditor : Editor {

    private LevelData data = null;

    void OnEnable()
    {
        data = (LevelData)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();


        if (data.previewLevel != null)
        {
            GUILayout.Label("Level Texture");
            GUILayout.Label(data.previewLevel, GUILayout.MaxHeight(250), GUILayout.MaxWidth(250));
        }



        if (GUILayout.Button("Update Preview", GUILayout.Height(40)))
        {
            data.CreateEditorPreviewTextures();
        }


        if (GUILayout.Button("Update To Generator", GUILayout.Height(40)))
        {
            data.NotifyOfUpdatedValues();
            EditorUtility.SetDirty(target);
        }

        if (GUILayout.Button("Random & Update", GUILayout.Height(40)))
        {
            GenerateNewSeeds();
            data.CreateEditorPreviewTextures();
            data.NotifyOfUpdatedValues();
            EditorUtility.SetDirty(target);
        }

    }

    void GenerateNewSeeds()
    {
        data.levelSeed = Random.Range(0, 1000000);

    }
}
