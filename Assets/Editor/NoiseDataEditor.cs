﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(NoiseData), true)]
public class NoiseDataEditor : Editor {
    
    private NoiseData data = null;

    void OnEnable()
    {
        data = (NoiseData)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        

        if (data.previewNoise != null)
        {
            GUILayout.Label(data.previewNoise, GUILayout.MaxHeight(250), GUILayout.MaxWidth(250));
        }

        if (GUILayout.Button("Update Preview", GUILayout.Height(40)))
        {
            data.CreateEditorPreviewTextures();
        }

        if (GUILayout.Button("Update To Generator", GUILayout.Height(40)))
        {
            data.NotifyOfUpdatedValues();
            EditorUtility.SetDirty(target);
        }

        if (GUILayout.Button("Random & Update", GUILayout.Height(40)))
        {
            GenerateNewSeed();
            data.CreateEditorPreviewTextures();
            data.NotifyOfUpdatedValues();
            EditorUtility.SetDirty(target);
        }
    }


    void GenerateNewSeed()
    {
        data.noiseSeed = Random.Range(0, 1000000);
    }
}
