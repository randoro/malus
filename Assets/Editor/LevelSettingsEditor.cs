﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LevelSettingsData), true)]
public class LevelSettingsEditor : Editor
{

    private LevelSettingsData data = null;

    void OnEnable()
    {
        data = (LevelSettingsData)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();


        if (data.previewLevel != null)
        {
            GUILayout.Label("Level Texture");
            GUILayout.Label(data.previewLevel, GUILayout.MaxHeight(250), GUILayout.MaxWidth(250));
        }



        if (GUILayout.Button("Random Preview", GUILayout.Height(40)))
        {
            data.CreateEditorPreviewTextures();
        }


        if (GUILayout.Button("Update To Generator", GUILayout.Height(40)))
        {
            data.NotifyOfUpdatedValues();
            EditorUtility.SetDirty(target);
        }
    }
}