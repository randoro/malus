﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(SplatRuleBase), true)]
public class SplatRuleBaseEditor : Editor
{
    private SplatRuleBase data = null;

    void OnEnable()
    {
        data = (SplatRuleBase)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        
        if (GUILayout.Button("Update To Generator"))
        {
            data.NotifyOfUpdatedValues();
            EditorUtility.SetDirty(target);
        }
    }

}
